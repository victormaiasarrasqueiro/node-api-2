module.exports = ((sequelize, Sequelize) => {
  const Sequence = sequelize.define('Sequence', {
    SEQUENCE_NAME: {
      type: Sequelize.STRING(250),
      allowNull: false,
      primaryKey: true,
    },
    SEQUENCE_CUR_VALUE: {
      type: Sequelize.INTEGER(20),
      allowNull: true,
    },
    SEQUENCE_CYCLE: {
      type: Sequelize.INTEGER(1),
      allowNull: true,
    },
    SEQUENCE_INCREMENTE: {
      type: Sequelize.INTEGER(20),
      allowNull: true,
    },
    SEQUENCE_MAX_VALUE: {
      type: Sequelize.INTEGER(20),
      allowNull: true,
    },
    SEQUENCE_MIN_VALUE: {
      type: Sequelize.INTEGER(20),
      allowNull: true,
    },
  }, {
    tableName: 'SEQUENCE',
  });


  return Sequence;
});

