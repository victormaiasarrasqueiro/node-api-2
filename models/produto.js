module.exports = ((sequelize, Sequelize) => {
  const produto = sequelize.define('produto', {
    ID_PRODUTO: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    NM_PRODUTO: {
      type: Sequelize.STRING(45),
      allowNull: false,
    },
    DS_PRODUTO_HTML: {
      type: Sequelize.TEXT,
      allowNull: false,
    },
    IS_ATIVO: {
      type: Sequelize.INTEGER(4),
      allowNull: false,
      defaultValue: '1',
    },
    DT_INCLUSAO: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.DATE,
    },
  }, {
    tableName: 'TB_PRODUTO',
  });

  produto.associate = (models) => {
    
  };

  return produto;
});
