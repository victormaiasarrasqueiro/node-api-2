module.exports = ((sequelize, Sequelize) => {
  const lojaProduto = sequelize.define('lojaProduto', {
    ID_LOJA_PRODUTO: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    DS_PRECO: {
      type: Sequelize.FLOAT,
      allowNull: false,
    },
    IS_ATIVO: {
      type: Sequelize.INTEGER(4),
      allowNull: false,
      defaultValue: '1',
    },
    DT_INCLUSAO: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.DATE,
    },
  }, {
    tableName: 'TB_LOJA_PRODUTO',
  });

  lojaProduto.associate = (models) => {
    models.loja.belongsToMany(models.produto, { through: models.lojaProduto, foreignKey: 'ID_LOJA', as: 'lojas' });
    models.produto.belongsToMany(models.loja, { through: models.lojaProduto, foreignKey: 'ID_PRODUTO', as: 'produtos' });
  };

  return lojaProduto;
});
