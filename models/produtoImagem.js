module.exports = ((sequelize, Sequelize) => {
  const produtoImagem = sequelize.define('produtoImagem', {
    ID_PRODUTO_IMAGEM: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    ID_IMAGEM: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'TB_IMAGEM',
        key: 'ID_IMAGEM',
      },
    },
    ID_PRODUTO: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'TB_PRODUTO',
        key: 'ID_PRODUTO',
      },
    },
    IS_IMAGEM_PRINCIPAL: {
      type: Sequelize.INTEGER(4),
      allowNull: false,
      defaultValue: '0',
    },
    DT_INCLUSAO: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.DATE,
    },
    IS_ATIVO: {
      type: Sequelize.INTEGER(4),
      allowNull: false,
      defaultValue: '1',
    },
  }, {
    tableName: 'TB_PRODUTO_IMAGEM',
  });

  produtoImagem.associate = (models) => {
    models.imagem.belongsToMany(models.produto, { through: models.produtoImagem, foreignKey: 'ID_IMAGEM', as: 'imagensProdutoImagem' });
    models.produto.belongsToMany(models.imagem, { through: models.produtoImagem, foreignKey: 'ID_PRODUTO', as: 'produtosProdutoImagem' });
  };

  return produtoImagem;
});
