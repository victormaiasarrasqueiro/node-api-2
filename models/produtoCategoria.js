module.exports = ((sequelize, Sequelize) => {
  const produtoCategoria = sequelize.define('produtoCategoria', {
    ID_PRODUTO_CATEGORIA: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    ID_PRODUTO: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'TB_PRODUTO',
        key: 'ID_PRODUTO',
      },
    },
    ID_CATEGORIA: {
      type: Sequelize.INTEGER(5),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'TB_CATEGORIA',
        key: 'ID_CATEGORIA',
      },
    },
    DT_INCLUSAO: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.DATE,
    },
  }, {
    tableName: 'TB_PRODUTO_CATEGORIA',
  });

  produtoCategoria.associate = (models) => {
    models.categoria.belongsToMany(models.produto, { through: models.produtoCategoria, foreignKey: 'ID_PRODUTO', as: 'categoriasProdutoCategorias' });
    models.produto.belongsToMany(models.categoria, { through: models.produtoCategoria, foreignKey: 'ID_CATEGORIA', as: 'produtosProdutoCategorias' });
  };

  return produtoCategoria;
});
