module.exports = ((sequelize, Sequelize) => {
  const Imagem = sequelize.define('imagem', {
    ID_IMAGEM: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    TP_IMAGEM: {
      type: Sequelize.STRING(5),
      allowNull: false,
      defaultValue: 'jpg',
      values: ['jpg', 'jpeg'],
    },
    ID_BUCKET: {
      type: Sequelize.INTEGER(2),
      allowNull: false,
      defaultValue: '1',
      values: ['1'], // Id do bucket TODO Ainda sera definido os Buckets na Amazon
    },
  }, {
    tableName: 'TB_IMAGEM',
  });

  return Imagem;
});
