module.exports = ((sequelize, Sequelize) => {
  const UsuarioHistoricoLogin = sequelize.define('UsuarioHistoricoLogin', {
    DT_LOGIN: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.DATE,
    },
    NU_IP: {
      type: Sequelize.STRING(12),
      allowNull: true,
    },
    ID_USUARIO: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      references: {
        model: 'TB_USUARIO',
        key: 'ID_USUARIO',
      },
    },
  }, {
    tableName: 'TB_USUARIO_HISTORICO_LOGIN',
  });

  return UsuarioHistoricoLogin;
});
