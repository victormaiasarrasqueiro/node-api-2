module.exports = ((sequelize, Sequelize) => {
  const produtoPropriedadeValor = sequelize.define('produtoPropriedadeValor', {
    ID_PRODUTO_PROPRIEDADE_VALOR: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    ID_PRODUTO: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'TB_PRODUTO',
        key: 'ID_PRODUTO',
      },
    },
    ID_PROPRIEDADE: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'TB_PROPRIEDADE',
        key: 'ID_PROPRIEDADE',
      },
    },
    DS_VALOR: {
      type: Sequelize.STRING(50),
      allowNull: false,
    },
    DT_INCLUSAO: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.DATE,
    },
  }, {
    tableName: 'TB_PRODUTO_PROPRIEDADE_VALOR',
  });

  produtoPropriedadeValor.associate = (models) => {
    models.propriedade.belongsToMany(models.produto, { through: models.produtoPropriedadeValor, foreignKey: 'ID_PRODUTO' });
    models.produto.belongsToMany(models.propriedade, { through: models.produtoPropriedadeValor, foreignKey: 'ID_PROPRIEDADE' });
  };

  return produtoPropriedadeValor;
});
