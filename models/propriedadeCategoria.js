module.exports = ((sequelize, Sequelize) => {
  const propriedadeCategoria = sequelize.define('propriedadeCategoria', {
    ID_PROPRIEDADE_CATEGORIA: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    ID_PROPRIEDADE: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'TB_PROPRIEDADE',
        key: 'ID_PROPRIEDADE',
      },
    },
    ID_CATEGORIA: {
      type: Sequelize.INTEGER(5),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'TB_CATEGORIA',
        key: 'ID_CATEGORIA',
      },
    },
    IS_ATIVO: {
      type: Sequelize.INTEGER(4),
      allowNull: false,
      defaultValue: '1',
    },
    DT_INCLUSAO: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.DATE,
    },
  }, {
    tableName: 'TB_PROPRIEDADE_CATEGORIA',
  });

  propriedadeCategoria.associate = (models) => {
    models.propriedade.belongsToMany(models.categoria, { through: models.propriedadeCategoria, foreignKey: 'ID_CATEGORIA' });
    models.categoria.belongsToMany(models.propriedade, { through: models.propriedadeCategoria, foreignKey: 'ID_PRODUTO' });
  };

  return propriedadeCategoria;
});
