module.exports = ((sequelize, Sequelize) => {
  const loja = sequelize.define('loja', {
    ID_LOJA: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    NM_FANTASIA: {
      type: Sequelize.STRING(25),
      allowNull: false,
    },
    NM_RAZAO_SOCIAL: {
      type: Sequelize.STRING(60),
      allowNull: true,
    },
    CD_CNPJ: {
      type: Sequelize.STRING(15),
      allowNull: true,
    },
    IMAGEM_ID: {
      type: Sequelize.BIGINT,
      allowNull: true,
      references: {
        model: 'TB_IMAGEM',
        key: 'ID_IMAGEM',
      },
    },
    DT_INCLUSAO: {
      type: Sequelize.DATE,
      allowNull: true,
      defaultValue: Sequelize.DATE,
    },
    IS_ATIVO: {
      type: Sequelize.INTEGER(4),
      allowNull: true,
      defaultValue: '0',
    },
  }, {
    tableName: 'TB_LOJA',
  });

  loja.associate = (models) => {
    loja.hasOne(models.lojaEndereco, {
      onDelete: 'CASCADE',
      allowNull: false,
      foreignKey: 'ID_LOJA',
    });

    loja.hasOne(models.lojaContato, {
      onDelete: 'CASCADE',
      allowNull: false,
      foreignKey: 'ID_LOJA',
    });
  };

  return loja;
});
