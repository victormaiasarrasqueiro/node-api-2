module.exports = ((sequelize, Sequelize) => {
  const lojaContato = sequelize.define('lojaContato', {
    ID_LOJA_CONTATO: {
      type: Sequelize.INTEGER(2),
      allowNull: false,
      primaryKey: true,
    },
    TP_MEIO_CONTATO: {
      type: Sequelize.INTEGER(2),
      allowNull: false,
      defaultValue: '1',
      values: ['1', '2', '3'], // 1-Email. 2-Celular. 3-Telefone. 4-WebsitePessoal 5-MercadoLivre
    },
    DS_VALOR: {
      type: Sequelize.STRING(45),
      allowNull: false,
    },
    IS_ATIVO: {
      type: Sequelize.INTEGER(1),
      allowNull: false,
      defaultValue: '1',
    },
    DT_INCLUSAO: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.DATE,
    },
    ID_LOJA: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'TB_LOJA',
        key: 'ID_LOJA',
      },
    },
  }, {
    tableName: 'TB_LOJA_CONTATO',
  });

  lojaContato.associate = (models) => {
    lojaContato.belongsTo(models.loja, {
      onDelete: 'CASCADE',
      allowNull: true,
      foreignKey: 'ID_LOJA',
    });
  };

  return lojaContato;
});
