module.exports = ((sequelize, Sequelize) => {
  const categoria = sequelize.define('categoria', {
    ID_CATEGORIA: {
      type: Sequelize.INTEGER(5),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    ID_CATEGORIA_PAI: {
      type: Sequelize.INTEGER(5),
      allowNull: true,
      references: {
        model: 'TB_CATEGORIA',
        key: 'ID_CATEGORIA',
      },
    },
    NM_CATEGORIA: {
      type: Sequelize.STRING(100),
      allowNull: false,
    },
    IS_ATIVO: {
      type: Sequelize.INTEGER(1),
      allowNull: false,
      defaultValue: '0',
    },
    DT_INCLUSAO: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.DATE,
    },
  }, {
    tableName: 'TB_CATEGORIA',
  });

  categoria.associate = (models) => {
    categoria.hasMany(models.categoria, {
      onDelete: 'CASCADE',
      allowNull: true,
      foreignKey: 'ID_CATEGORIA_PAI',
    });
  };

  return categoria;
});
