module.exports = ((sequelize, Sequelize) => {
  const lojaEndereco = sequelize.define('lojaEndereco', {
    ID_LOJA_ENDERECO: {
      type: Sequelize.INTEGER(2),
      allowNull: false,
      primaryKey: true,
    },
    NU_CEP: {
      type: Sequelize.STRING(10),
      allowNull: false,
    },
    DS_ENDERECO: {
      type: Sequelize.STRING(25),
      allowNull: false,
    },
    NU_LOGRADOURO: {
      type: Sequelize.STRING(5),
      allowNull: false,
    },
    NM_CIDADE: {
      type: Sequelize.STRING(25),
      allowNull: false,
    },
    NM_ESTADO: {
      type: Sequelize.STRING(5),
      allowNull: false,
    },
    NM_PAIS: {
      type: Sequelize.STRING(15),
      allowNull: false,
      values: ['BR'],
    },
    CD_X: {
      type: Sequelize.DOUBLE,
      allowNull: false,
    },
    CD_Y: {
      type: Sequelize.DOUBLE,
      allowNull: false,
    },
    NM_BAIRRO: {
      type: Sequelize.STRING(15),
      allowNull: false,
    },
    DS_COMPLEMENTO: {
      type: Sequelize.STRING(15),
      allowNull: true,
    },
    IS_ATIVO: {
      type: Sequelize.INTEGER(4),
      allowNull: false,
      defaultValue: '1',
    },
    DT_INCLUSAO: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.DATE,
    },
    ID_LOJA: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'TB_LOJA',
        key: 'ID_LOJA',
      },
    },
  }, {
    tableName: 'TB_LOJA_ENDERECO',
  });

  lojaEndereco.associate = (models) => {
    lojaEndereco.belongsTo(models.loja, {
      onDelete: 'CASCADE',
      allowNull: true,
      foreignKey: 'ID_LOJA',
    });
  };

  return lojaEndereco;
});
