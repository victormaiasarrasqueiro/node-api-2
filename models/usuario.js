module.exports = ((sequelize, Sequelize) => {
  const Usuario = sequelize.define('usuario', {
    ID_USUARIO: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    NM_USUARIO: {
      type: Sequelize.STRING(45),
      allowNull: false,
    },
    DS_EMAIL: {
      type: Sequelize.STRING(60),
      allowNull: false,
      unique: true,
    },
    CD_CPF: {
      type: Sequelize.STRING(15),
      allowNull: false,
      unique: true,
    },
    CD_PASSWORD: {
      type: Sequelize.STRING(50),
      allowNull: false,
    },
    IS_ATIVO: {
      type: Sequelize.INTEGER(4),
      allowNull: false,
    },
    DT_INCLUSAO: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.DATE,
    },
    NU_CELULAR: {
      type: Sequelize.STRING(15),
      allowNull: false,
      unique: true,
    },
  }, {
    tableName: 'TB_USUARIO',
  });

  return Usuario;
});
