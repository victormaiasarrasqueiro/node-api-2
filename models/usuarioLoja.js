module.exports = ((sequelize, Sequelize) => {
  const UsuarioLoja = sequelize.define('UsuarioLoja', {
    ID_USUARIO: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'TB_USUARIO',
        key: 'ID_USUARIO',
      },
    },
    ID_LOJA: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'TB_LOJA',
        key: 'ID_LOJA',
      },
    },
    IS_ADMIN: {
      type: Sequelize.INTEGER(4),
      allowNull: false,
      defaultValue: '0',
    },
    IS_ATIVO: {
      type: Sequelize.INTEGER(4),
      allowNull: false,
      defaultValue: '1',
    },
  }, {
    tableName: 'TB_USUARIO_LOJA',
  });

  return UsuarioLoja;
});
