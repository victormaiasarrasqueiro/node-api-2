module.exports = ((sequelize, Sequelize) => {
  const propriedadeValorDefault = sequelize.define('propriedadeValorDefault ', {
    ID_PROPRIEDADE: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      references: {
        model: 'TB_PROPRIEDADE',
        key: 'ID_PROPRIEDADE',
      },
    },
    DS_VALOR: {
      type: Sequelize.STRING(50),
      allowNull: false,
    },
    IS_ATIVO: {
      type: Sequelize.INTEGER(4),
      allowNull: false,
      defaultValue: '1',
    },
    DT_INCLUSAO: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.DATE,
    },
  }, {
    tableName: 'TB_PROPRIEDADE_VALOR_DEFAULT',
  });

  return propriedadeValorDefault;
});
