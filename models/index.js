const sequelize = require('../config/db/mysql/dbMysql');
const fs = require('fs');

const db = {};

// Sync all models that aren't already in the database
 //sequelize.getConnection().sync({ force: true });

fs.readdirSync(__dirname)
  .filter(file => (file.indexOf('.') !== 0) && (file !== 'index.js'))
  .forEach((file) => {
    const model = sequelize.getConnection().import(`${__dirname}/${file}`);
    db[model.name] = model;
  });

Object.keys(db).forEach((modelName) => {
  if ('associate' in db[modelName]) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;

module.exports = db;
