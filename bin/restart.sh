#CONSTANTES
API_URL08=http://poapx08.interno:8091/
API_URL09=http://poapx09.interno:8091/
API_URL20A=http://poapx20a.interno:8091/
API_URL20B=http://poapx20b.interno:8091/
API_URL20C=http://poapx20c.interno:8091/

echo 'Executando o restart do PRÉ-CADASTRO em poapx08'
ssh dvopsusr@poapx08 "sudo /usr/local/bin/docker-compose -p production -f /precadastro/bin/production/docker-compose.yml restart app"

echo 'Aguardando 20 segundos...'
sleep 20s
echo 'Verificando API PRÉ-CADASTRO em poapx08...'
if [ $(curl --write-out %{http_code} --connect-timeout 9 --max-time 9 --output /dev/null $API_URL08) -eq 200 ]; 
then
    echo 'API PRÉ-CADASTRO em poapx08 respondeu com SUCESSO - 200 OK'
    echo 'Executando o restart do PRÉ-CADASTRO em poapx09'
    ssh dvopsusr@poapx09 "sudo /usr/local/bin/docker-compose -p production -f /precadastro/bin/production/docker-compose.yml restart app"
else
   echo 'API em PRÉ-CADASTRO em poapx08 não respondeu'	
   echo 'ERRO'
fi

echo 'Aguardando 20 segundos...'
sleep 20s
echo 'Verificando API PRÉ-CADASTRO em poapx09...'
if [ $(curl --write-out %{http_code} --connect-timeout 9 --max-time 9 --output /dev/null $API_URL09) -eq 200 ];
then
   echo 'API PRÉ-CADASTRO em poapx09 respondeu com SUCESSO - 200 OK'
   echo 'Executando o restart do PRÉ-CADASTRO em poapx20a'
    ssh dvopsusr@poapx20a "sudo /usr/local/bin/docker-compose -p production -f /precadastro/bin/production/docker-compose.yml restart app"
else
   echo 'PRÉ-CADASTRO em poapx09 não respondeu'    
   echo 'ERRO'
fi

echo 'Aguardando 20 segundos...'
sleep 20s
echo 'Verificando API PRÉ-CADASTRO em poapx20a...'
if [ $(curl --write-out %{http_code} --connect-timeout 9 --max-time 9 --output /dev/null $API_URL20A) -eq 200 ];
then
   echo 'API PRÉ-CADASTRO em poapx20a respondeu com SUCESSO - 200 OK'
   echo 'Executando o restart do PRÉ-CADASTRO em poapx20b'
    ssh dvopsusr@poapx20b "sudo /usr/local/bin/docker-compose -p production -f /precadastro/bin/production/docker-compose.yml restart app"
else
   echo 'PRÉ-CADASTRO em poapx20a não respondeu'    
   echo 'ERRO'
fi

echo 'Aguardando 20 segundos...'
sleep 20s
echo 'Verificando API PRÉ-CADASTRO em poapx20b...'
if [ $(curl --write-out %{http_code} --connect-timeout 9 --max-time 9 --output /dev/null $API_URL20B) -eq 200 ];
then
   echo 'API PRÉ-CADASTRO em poapx20b respondeu com SUCESSO - 200 OK'
   echo 'Executando o restart do PRÉ-CADASTRO em poapx20c'
    ssh dvopsusr@poapx20c "sudo /usr/local/bin/docker-compose -p production -f /precadastro/bin/production/docker-compose.yml restart app"
else
   echo 'PRÉ-CADASTRO em poapx20b não respondeu'    
   echo 'ERRO'
fi

echo 'Aguardando 20 segundos...'
sleep 20s
echo 'Verificando API PRÉ-CADASTRO em poapx20c...'
if [ $(curl --write-out %{http_code} --connect-timeout 9 --max-time 9 --output /dev/null $API_URL20C) -eq 200 ];
then
   echo 'API PRÉ-CADASTRO em poapx20c respondeu com SUCESSO - 200 OK'
   echo 'Restart do PRÉ-CADASTRO em poapx08, poapx09, poapx20a, poapx20b e poapx20c executado com sucesso'
else
   echo 'PRÉ-CADASTRO em poapx20c não respondeu'    
   echo 'ERRO'
fi

exit