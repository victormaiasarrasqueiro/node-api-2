#!/bin/bash

# garantindo que os logs antigos serão apagados para que o filebeat não processe novamente os mesmos logs
rm -rf /precadastro/logs/{*.log*,*.txt*}

sudo /usr/local/bin/docker-compose -f $BACKEND_PATH/bin/staging/docker-compose.yml up --no-build --force-recreate -d
sudo /usr/local/bin/docker-compose -f $BACKEND_PATH/bin/staging/docker-compose.yml scale app=1