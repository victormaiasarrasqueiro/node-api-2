#!/bin/bash

if [ "$1" != "" ]; then
    URL=$1
else
    URL=http://localhost:8090
fi

FLAG=0
COUNT=0
WAITTIME=5

while [ $FLAG -eq 0 ]
do 
    sleep $WAITTIME
    FLAG=$(curl -sf $URL | grep "PreCadastro-Backend" | wc -l)
    
    echo 'Aguardando URL: '$URL
    COUNT=$((COUNT+$WAITTIME))
    echo $COUNT

    if [ $COUNT -gt 120 ]
    then
        exit 2
    fi  
done
