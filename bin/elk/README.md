# ELK com KOPF

Dockerfile baseado na imagem https://hub.docker.com/r/sebp/elk/ com a inclusão do plugin KOPF no elastic search.

# Instalação

1 - Rode o plugin compila.sh para gerar a nova imagem
2 - Rode o script run.sh para iniciar a nova imagem

# KOPF

O Kopf é um plugin que permite ver a saude do cluster de ES assim como interagir com o elasticsearch através de um console REST

Acesse em:

`http://localhost:9200/_plugin/kopf`

# Dúvidas

Qualquer dúvida entrar em contato com diogo.leitao@oi.net.br