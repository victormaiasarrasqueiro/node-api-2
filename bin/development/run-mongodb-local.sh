sudo docker run -p 27017:27017 --name mongodb_minhaoi -v /mongo/data/db:/data/db -d mongo
sudo docker run -it -d \
    --name mongo-express_minhaoi \
    --link mongodb_minhaoi:mongo \
    -p 8081:8081 mongo-express