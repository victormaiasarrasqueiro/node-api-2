

module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.bulkInsert('TB_LOJA_ENDERECO', [
      {
        ID_LOJA_ENDERECO: 1,
        NU_CEP: '24451045',
        DS_ENDERECO: 'Av. Rio Branco st2',
        NU_LOGRADOURO: 156,
        NM_BAIRRO: 'Centro',
        NM_CIDADE: 'Rio de Janeiro',
        NM_ESTADO: 'RJ',
        NM_PAIS: 'Brasil',
        CD_X: 1332356,
        CD_Y: 1332356,
        ID_LOJA: 1,
        DT_INCLUSAO: new Date(),
        IS_ATIVO: 1,
      },
      {
        ID_LOJA_ENDERECO: 2,
        NU_CEP: '24451045',
        DS_ENDERECO: 'Av. Rio Branco st2',
        NU_LOGRADOURO: 156,
        NM_BAIRRO: 'Centro',
        NM_CIDADE: 'Rio de Janeiro',
        NM_ESTADO: 'RJ',
        NM_PAIS: 'Brasil',
        CD_X: 1332356,
        CD_Y: 1332356,
        ID_LOJA: 2,
        DT_INCLUSAO: new Date(),
        IS_ATIVO: 1,
      }], {}),


  down: (queryInterface, Sequelize) =>
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example: */
    queryInterface.bulkDelete('TB_LOJA_ENDERECO', null, {})
  ,
};
