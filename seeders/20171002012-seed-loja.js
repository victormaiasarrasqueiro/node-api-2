

module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.bulkInsert('TB_LOJA', [{
      ID_LOJA: 1,
      NM_FANTASIA: 'Smart Games',
      NM_RAZAO_SOCIAL: 'Ego jogos Ltda',
      CD_CNPJ: '23213132',
      DT_INCLUSAO: new Date(),
    },
    {
      ID_LOJA: 2,
      NM_FANTASIA: 'Cell Games',
      NM_RAZAO_SOCIAL: 'Jogos Ltda',
      CD_CNPJ: '13213132',
      DT_INCLUSAO: new Date(),
    }], {}),


  down: (queryInterface, Sequelize) => 
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example: */
     queryInterface.bulkDelete('TB_LOJA', null, {})
  ,
};
