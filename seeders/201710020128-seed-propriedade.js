

module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.bulkInsert('TB_PROPRIEDADE', [
      {
        ID_PROPRIEDADE: 1,
        NM_PROPRIEDADE: 'Fabricante',
        CD_PROPRIEDADE: 'FAB-GERAL',
        DT_INCLUSAO: new Date(),
        IS_ATIVO: 1,
      },
      {
        ID_PROPRIEDADE: 2,
        NM_PROPRIEDADE: 'Fabricante',
        CD_PROPRIEDADE: 'FAB-Pendriver',
        DT_INCLUSAO: new Date(),
        IS_ATIVO: 1,
      },
      {
        ID_PROPRIEDADE: 3,
        NM_PROPRIEDADE: 'Potência',
        CD_PROPRIEDADE: 'POT-SOM',
        DT_INCLUSAO: new Date(),
        IS_ATIVO: 1,
      },
      {
        ID_PROPRIEDADE: 4,
        NM_PROPRIEDADE: 'Potência',
        CD_PROPRIEDADE: 'POT-ELETRICA',
        DT_INCLUSAO: new Date(),
        IS_ATIVO: 1,
      },
      {
        ID_PROPRIEDADE: 5,
        NM_PROPRIEDADE: 'Modelo',
        CD_PROPRIEDADE: 'MODELO-SMARTPHONE',
        DT_INCLUSAO: new Date(),
        IS_ATIVO: 1,
      },
      {
        ID_PROPRIEDADE: 6,
        NM_PROPRIEDADE: 'Modelo',
        CD_PROPRIEDADE: 'MODELO',
        DT_INCLUSAO: new Date(),
        IS_ATIVO: 1,
      },
      {
        ID_PROPRIEDADE: 7,
        NM_PROPRIEDADE: 'Marca',
        CD_PROPRIEDADE: 'MARCA',
        DT_INCLUSAO: new Date(),
        IS_ATIVO: 1,
      },
      {
        ID_PROPRIEDADE: 8,
        NM_PROPRIEDADE: 'Polegadas',
        CD_PROPRIEDADE: 'POLEGS',
        DT_INCLUSAO: new Date(),
        IS_ATIVO: 1,
      },
      {
        ID_PROPRIEDADE: 9,
        NM_PROPRIEDADE: 'Tamanho',
        CD_PROPRIEDADE: 'TAMANHO',
        DT_INCLUSAO: new Date(),
        IS_ATIVO: 1,
      },
      {
        ID_PROPRIEDADE: 10,
        NM_PROPRIEDADE: 'Espaço em Disco',
        CD_PROPRIEDADE: 'ESPACO_EM_DISCO',
        DT_INCLUSAO: new Date(),
        IS_ATIVO: 1,
      },
      {
        ID_PROPRIEDADE: 11,
        NM_PROPRIEDADE: 'Cor',
        CD_PROPRIEDADE: 'COR',
        DT_INCLUSAO: new Date(),
        IS_ATIVO: 1,
      },
      {
        ID_PROPRIEDADE: 12,
        NM_PROPRIEDADE: 'Sistema Operacional',
        CD_PROPRIEDADE: 'SIST_OPERACIONAL',
        DT_INCLUSAO: new Date(),
        IS_ATIVO: 1,
      },
      {
        ID_PROPRIEDADE: 13,
        NM_PROPRIEDADE: 'Velocidade',
        CD_PROPRIEDADE: 'VELOCIDADE',
        DT_INCLUSAO: new Date(),
        IS_ATIVO: 1,
      },
    ], {}),


  down: (queryInterface, Sequelize) =>
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example: */
    queryInterface.bulkDelete('TB_PROPRIEDADE', null, {})
  ,
};
