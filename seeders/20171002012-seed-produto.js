

module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.bulkInsert('TB_PRODUTO', [{
      ID_PRODUTO: 1,
      NM_PRODUTO: 'Monitor/TV Samsung LT300 29 Smart FULL HD',
      DS_PRODUTO_HTML: 'Novo <b>Monitor</b> maskarade para hackers<br><br>Melhor Monitor/TV do Mundo',
      DT_INCLUSAO: new Date(),
    },
    {
      ID_PRODUTO: 2,
      NM_PRODUTO: 'Pendriver Scandisk 16GB RedNose 6443',
      DS_PRODUTO_HTML: 'Novo jogo pc maskarade <i>para</i> hackers',
      DT_INCLUSAO: new Date(),
    },
    {
      ID_PRODUTO: 3,
      NM_PRODUTO: 'Pendriver Scandisk 32GB RedNose 883738',
      DS_PRODUTO_HTML: 'Novo jogo pc maskarade <i>para</i> hackers',
      DT_INCLUSAO: new Date(),
    },
    {
      ID_PRODUTO: 4,
      NM_PRODUTO: 'Pendriver Scandisk 128GB RedNose 757657',
      DS_PRODUTO_HTML: 'Novo jogo pc maskarade <i>para</i> hackers',
      DT_INCLUSAO: new Date(),
    },
    {
      ID_PRODUTO: 5,
      NM_PRODUTO: 'Caixa de Som Lenovo StarterPlus 23453 ',
      DS_PRODUTO_HTML: '<h4>Novo jogo pc maskarade para hackers</h4>',
      DT_INCLUSAO: new Date(),
    }], {}),


  down: (queryInterface, Sequelize) =>
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example: */
    queryInterface.bulkDelete('TB_PRODUTO', null, {})
  ,
};
