module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.bulkInsert('TB_LOJA_CONTATO', [{
      ID_LOJA_CONTATO: 1,
      TP_MEIO_CONTATO: 1,
      DS_VALOR: '21992992720',
      ID_LOJA: 1,
      DT_INCLUSAO: new Date(),
      IS_ATIVO: 1,
    },
    {
      ID_LOJA_CONTATO: 2,
      TP_MEIO_CONTATO: 2,
      DS_VALOR: 'loja@loja.com.br',
      ID_LOJA: 2,
      DT_INCLUSAO: new Date(),
      IS_ATIVO: 1,
    }], {}),


  down: (queryInterface, Sequelize) =>
  /*
        Add reverting commands here.
        Return a promise to correctly handle asynchronicity.
  
        Example: */
    queryInterface.bulkDelete('TB_LOJA_CONTATO', null, {})
  ,
};

