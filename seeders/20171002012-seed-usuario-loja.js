

module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.bulkInsert('TB_USUARIO_LOJA', [
      {
        ID_USUARIO: 1,
        ID_LOJA: 1,
        IS_ADMIN: 1,
        IS_ATIVO: 1,
      },
      {
        ID_USUARIO: 1,
        ID_LOJA: 2,
        IS_ADMIN: 0,
        IS_ATIVO: 1,
      },
      {
        ID_USUARIO: 2,
        ID_LOJA: 1,
        IS_ADMIN: 0,
        IS_ATIVO: 1,
      },
      {
        ID_USUARIO: 2,
        ID_LOJA: 2,
        IS_ADMIN: 1,
        IS_ATIVO: 1,
      },
    ], {}),


  down: (queryInterface, Sequelize) =>
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example: */
    queryInterface.bulkDelete('TB_USUARIO_LOJA', null, {})
  ,
};
