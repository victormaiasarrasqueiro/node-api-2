

module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.bulkInsert('TB_PRODUTO_CATEGORIA', [{
      ID_PRODUTO: 1,
      ID_CATEGORIA: 37,
      DT_INCLUSAO: new Date(),
    },
    {
      ID_PRODUTO: 1,
      ID_CATEGORIA: 39,
      DT_INCLUSAO: new Date(),
    },
    {
      ID_PRODUTO: 2,
      ID_CATEGORIA: 15,
      DT_INCLUSAO: new Date(),
    },
    {
      ID_PRODUTO: 3,
      ID_CATEGORIA: 15,
      DT_INCLUSAO: new Date(),
    },
    {
      ID_PRODUTO: 4,
      ID_CATEGORIA: 15,
      DT_INCLUSAO: new Date(),
    },
    {
      ID_PRODUTO: 5,
      ID_CATEGORIA: 27,
      DT_INCLUSAO: new Date(),
    },
    ], {}),


  down: (queryInterface, Sequelize) =>
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example: */
    queryInterface.bulkDelete('TB_PRODUTO_CATEGORIA', null, {})
  ,
};
