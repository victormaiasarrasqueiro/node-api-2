

module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.bulkInsert('TB_PROPRIEDADE_VALOR_DEFAULT', [{
      ID_PROPRIEDADE: 12,
      DS_VALOR: 'Windows',
      DT_INCLUSAO: new Date(),
      IS_ATIVO: 1,
    },
    {
      ID_PROPRIEDADE: 12,
      DS_VALOR: 'Mac OS',
      DT_INCLUSAO: new Date(),
      IS_ATIVO: 1,
    },
    {
      ID_PROPRIEDADE: 10,
      DS_VALOR: '128 GB',
      DT_INCLUSAO: new Date(),
      IS_ATIVO: 1,
    },
    {
      ID_PROPRIEDADE: 10,
      DS_VALOR: '256 GB',
      DT_INCLUSAO: new Date(),
      IS_ATIVO: 1,
    },
    {
      ID_PROPRIEDADE: 10,
      DS_VALOR: '512 GB',
      DT_INCLUSAO: new Date(),
      IS_ATIVO: 1,
    },
    {
      ID_PROPRIEDADE: 7,
      DS_VALOR: 'Apple',
      DT_INCLUSAO: new Date(),
      IS_ATIVO: 1,
    },
    {
      ID_PROPRIEDADE: 7,
      DS_VALOR: 'Sony',
      DT_INCLUSAO: new Date(),
      IS_ATIVO: 1,
    },
    {
      ID_PROPRIEDADE: 7,
      DS_VALOR: 'AOC',
      DT_INCLUSAO: new Date(),
      IS_ATIVO: 1,
    },
    ], {}),


  down: (queryInterface, Sequelize) =>
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example: */
    queryInterface.bulkDelete('TB_PROPRIEDADE_VALOR_DEFAULT', null, {})
  ,
};
