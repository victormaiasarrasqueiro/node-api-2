

module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.bulkInsert('TB_USUARIO', [{
      ID_USUARIO: 1,
      NM_USUARIO: 'Victor',
      DS_EMAIL: 'victor@bd.com',
      CD_CPF: '23213132',
      CD_PASSWORD: '123456',
      NU_CELULAR: '21992992720',
      IS_ATIVO: 1,
      DT_INCLUSAO: new Date(),
    },
    {
      ID_USUARIO: 2,
      NM_USUARIO: 'Wallace',
      DS_EMAIL: 'wallace@bd.com',
      CD_CPF: '23213122',
      CD_PASSWORD: '123456',
      NU_CELULAR: '21992992722',
      IS_ATIVO: 1,
      DT_INCLUSAO: new Date(),
    }], {}),


  down: (queryInterface, Sequelize) =>
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example: */
    queryInterface.bulkDelete('TB_USUARIO', null, {})
  ,
};
