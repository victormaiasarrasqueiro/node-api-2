

module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.bulkInsert('TB_LOJA_PRODUTO', [{
      ID_LOJA: 1,
      ID_PRODUTO: 1,
      DS_PRECO: 3.5,
      DT_INCLUSAO: new Date(),
      IS_ATIVO: 1,
    },
    {
      ID_LOJA: 2,
      ID_PRODUTO: 1,
      DS_PRECO: 3.6,
      DT_INCLUSAO: new Date(),
      IS_ATIVO: 1,
    }], {}),


  down: (queryInterface, Sequelize) =>
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example: */
    queryInterface.bulkDelete('TB_LOJA_PRODUTO', null, {})
  ,
};
