

module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.bulkInsert('TB_PROPRIEDADE_CATEGORIA', [{
      ID_PROPRIEDADE: 1,
      ID_CATEGORIA: 1,
      DT_INCLUSAO: new Date(),
    },
    {
      ID_PROPRIEDADE: 1,
      ID_CATEGORIA: 2,
      DT_INCLUSAO: new Date(),
    }], {}),


  down: (queryInterface, Sequelize) =>
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example: */
    queryInterface.bulkDelete('TB_PROPRIEDADE_CATEGORIA', null, {})
  ,
};
