const crypto = require('crypto');
const ursa = require('ursa');
const fs = require('fs');

const algorithm = 'bf-ecb';

module.exports = encryption;

function encryption() {
  const self = this;

  self.encrypt = function (data, key) {
    const cipher = crypto.createCipheriv(algorithm, Buffer(key), '');
    cipher.setAutoPadding(false);
    try {
      return Buffer(cipher.update(pad(data), 'utf8', 'binary') + cipher.final('binary'), 'binary').toString('base64');
    } catch (e) {
      return null;
    }
  };

  self.decrypt = function (data, key) {
    const decipher = crypto.createDecipheriv(algorithm, Buffer(key), '');
    decipher.setAutoPadding(false);
    try {
      return (decipher.update(Buffer(data, 'base64').toString('binary'), 'binary', 'utf8') + decipher.final('utf8')).replace(/\x00+$/g, '');
    } catch (e) {
      return null;
    }
  };

  self.encryptRSA = function (password) {
    const filePublicKey = fs.readFileSync('util/keys/pubkey.pub');
    const publicKey = ursa.createPublicKey(filePublicKey, 'utf8');
    try {
      return (publicKey.encrypt(password, 'utf8', 'base64', ursa.RSA_PKCS1_PADDING));
    } catch (e) {
      return null;
    }
  };
}

function pad(text) {
  const pad_bytes = 8 - (text.length % 8);
  for (let x = 1; x <= pad_bytes; x++) {
    text += String.fromCharCode(0);
  }
  return text;
}

