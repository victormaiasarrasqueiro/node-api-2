
module.exports = CPF;

function CPF() {
  const defaultMask = 'xxx.xxx.xxx-xx';
  const defaultPlaceholder = 'x';

  this.validar = function (cpf) {
    // 'use strict';

    let j = -1;
    let i;
    let add;
    let rev;

    cpf = cpf.replace(/[^\d]+/g, '');

    if (cpf === '' || cpf.length !== 11 || /^(\d)\1{10}$/.test(cpf)) {
      return false;
    }

    while (++j < 2) {
      add = 0;
      i = -1;

      while (++i < (9 + j)) {
        add += (cpf[i] >>> 0) * ((10 + j) - i);
      }

      rev = 11 - (add % 11);

      if (rev === 10 || rev === 11) {
        rev = 0;
      }

      if (rev !== cpf[9 + j] >>> 0) {
        return false;
      }
    }

    return true;
  };

  this.gerar = function (mask, placeholder) {
    // 'use strict';
    const numbers = [];
    let last;
    let result;

    while (numbers.length < 9) {
      numbers[numbers.length] = _random(9);
    }

    while (numbers.length < 11) {
      last = 11 - _mod(_sumNumbers(numbers), 11);

      if (last >= 10) {
        last = 0;
      }

      numbers[numbers.length] = last;
    }

    result = numbers.join('');

    if (typeof mask === 'boolean' && mask) {
      mask = defaultMask;
    }

    if (mask && mask.length) {
      if (typeof placeholder === 'undefined') {
        placeholder = defaultPlaceholder;
      }

      if (mask.match(new RegExp(placeholder, 'g')).length < 11) {
        throw new Error('O CPF deve conter 11 caracteres');
      }

      const placeholderRegex = new RegExp(placeholder);
      let i = -1;

      while (++i < 11) {
        mask = mask.replace(placeholderRegex, result[i]);
      }

      result = mask;
    }

    function _mod(dividend, divisor) {
      return Math.round(dividend - (Math.floor(dividend / divisor) * divisor));
    }

    function _random(n) {
      return Math.round(Math.random() * n);
    }

    function _sumNumbers(numbers) {
      return numbers.slice().reverse().reduce((a, b, i) => a + (b * (i + 2)), 0);
    }

    return result;
  };
}
