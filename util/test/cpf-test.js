let expect = require('chai').expect,
  cpf = require('../cpf');

describe('CPF Testes', () => {
  let e;

  beforeEach(() => {
    e = new cpf();
  });

  it('Mesmo sem passar a mascara seve usar uma mascara padrao para gerar', (done) => {
    expect(e.gerar(true, undefined).length).to.be.equal(14);
    done();
  });

  it('Validar cpf', (done) => {
    expect(e.validar('123.456.789-10')).to.be.equal(false);
    expect(e.validar('123.456.x89-10')).to.be.equal(false);
    expect(e.validar('645.746.956-68')).to.be.equal(true);
    expect(e.validar('645.746.9-68')).to.be.equal(false);
    expect(e.validar('')).to.be.equal(false);

    done();
  });


  it('Validar formato cpf', (done) => {
    expect(e.validar(e.gerar('xxx.xxx.xxx-xx', 'x'))).to.be.equal(true);
    expect(e.validar(e.gerar('xxx.xxx.xxx-xx', 'x'))).to.be.equal(true);
    expect(e.validar(e.gerar('xxxxxxxxxxx', 'x'))).to.be.equal(true);

    const p1 = new Promise(((resolve, reject) => {
      resolve('Sucesso');
    }));

    p1.then((value) => {
      console.log(value);
      expect(e.validar(e.gerar('xxxxxxxxx', undefined))).to.be.false(true);
    }).catch((exp) => {
      expect(e.validar('49303945700')).to.be.equal(true);
      expect(e.validar('00668582677')).to.be.equal(true);
    }).then(() => {
      expect(e.validar(e.gerar('xxxxxxxxxxx', 'x'))).to.be.equal(true);
    });

    done();
  });
});
