const cameldeep = require('./camelcase-keys-deep');
const constant = require('../config/constants');

class Utils {
  static montaJsonMsgRetorno(pCode, pMessage, pData) {
    const ret = {
      code: pCode,
      message: pMessage,
      data: pData,
    };
    return ret;
  }

  static camelDeep(obj) {
    return cameldeep(obj);
  }

  static camelDeepSequelize(obj) {
    return cameldeep(JSON.parse(JSON.stringify(obj)));
  }

  static successJson(value) {
    return this.montaJsonMsgRetorno(constant.CODE_200, constant.SUCCESS,
      this.camelDeepSequelize(value));
  }

  static invalidJson(value) {
    return this.montaJsonMsgRetorno(constant.CODE_400, constant.INVALID, value);
  }

  static errorJson(value) {
    return this.montaJsonMsgRetorno(constant.CODE_500, constant.ERROR, value);
  }
}

module.exports = Utils;
