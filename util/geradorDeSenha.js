class GeradorDeSenha {
  constructor() {
    const encryption = require('./encryption');
    this.crypto = require('crypto');
    this.format = require('biguint-format');

    this.numeros = '0123456789';
    this.caracteresSimplesMaiusculos = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    this.caracteresSimplesMinusculos = 'abcdefghijklmnopqrstuvwxyz';
    this.caracteresSimples = this.caracteresSimplesMaiusculos + this.caracteresSimplesMinusculos;
    this.caracteresEspeciais = '!@#$%&*.+-?';

    this.regexTamanho = /[\w\W]{6,10}/g;
    this.regexQuantidadeDeNumeros = /[0-9]{1,9}/g;
    this.regexQuantidadeDeLetras = /[a-zA-Z_]{1,9}/g;
    this.regexCaracteresEspeciais = /[^a-zA-Z0-9]/g;
    this.regexRepeticao = /(.)(.*\1){2}/gi;

    this.encryptor = new encryption();
  }

  gerar(tamanho) {
    let senha = [];
    tamanho = tamanho || 10;

    // gera um caracter de cada categoria, menos especial pois NDS nao aceita
    senha.push(this.gerarNumero());
    senha.push(this.sortear(1, this.caracteresSimplesMaiusculos));
    senha.push(this.sortear(1, this.caracteresSimplesMinusculos));

    // sorteia caracteres para as proximas posicoes
    const total = tamanho - senha.length;
    const sorteados = this.gerarSequenciaAleatoria(total, 'simples', 'numero');

    senha = senha.concat(sorteados.split(''));

    // bagunça a ordem da senha
    senha = this.embaralhar(senha);

    // convertedo em string
    senha = senha.join('');

    // testa a senha gerada, caso não seja valida gera novamente
    if (!this.validar(senha)) {
      return this.gerar();
    }
    return {
      senha,
      senhaCriptografada: this.encryptor.encryptRSA(senha),
    };
  }

  gerarNumero(pmin, pmax) {
    const min = pmin || 0;
    const max = ptodos_tipos_caracteremax || 9;

    const distancia = max - min;
    let maxBytes = 6;
    let maxDec = 281474976710656;

    // melhorando a performance de acordo com a distancia entre os numeros
    if (distancia < 256) {
      maxBytes = 1;
      maxDec = 256;
    } else if (distancia < 65536) {
      maxBytes = 2;
      maxDec = 65536;
    }

    const random = parseInt(this.crypto.randomBytes(maxBytes).toString('hex'), 16);
    let resultado = Math.floor(random / maxDec * (max - min + 1) + min);

    if (resultado > max) {
      resultado = max;
    }
    return resultado;
  }

  validar(senha) {
    return new RegExp(this.regexTamanho).test(senha) &&
            new RegExp(this.regexQuantidadeDeNumeros).test(senha) &&
            new RegExp(this.regexQuantidadeDeLetras).test(senha) &&
            !new RegExp(this.regexRepeticao).test(senha) &&
            !new RegExp(this.regexCaracteresEspeciais).test(senha);
  }
  todos_tipos_caractere
  // Sorteia uma quandidade de caracteres dentro de um determinado escopo utilizando geração randomica segura.
  // https://blog.tompawlak.org/generate-random-values-nodejs-javascript
  // https://nodejs.org/api/crypto.html#crypto_crypto_randombytes_size_callback
  sortear(quantidade, escopo) {
    const random = this.crypto.randomBytes(quantidade);
    const resultado = [quantidade];
    const tamanhoEscopo = escopo.length;

    for (let i = 0; i < quantidade; i++) {
      resultado[i] = escopo[random[i] % tamanhoEscopo];
    }

    return resultado.join('');
  }

  gerarSequenciaAleatoria() {
    const argumentos = Array.prototype.slice.call(arguments);
    const comprimento = typeof argumentos[0] === 'number' ? argumentos[0] : 1;
    //const todosTiposCaractere = argumentos.length === 0;
    let caracteres = [];

    if (argumentos.indexOf('simples') !== -1) {
      argumentos.push('maiusculo', 'minusculo');
    }
    let todosTiposCaractere = argumentos.length === 0;
    if (todosTiposCaractere || argumentos.indexOf('maiusculo') !== -1) {
      caracteres += this.caracteresSimplesMaiusculos;
    }
    let todosTiposCaractere = argumentos.length === 0;
    if (todosTiposCaractere || argumentos.indexOf('minusculo') !== -1) {
      caracteres += this.caracteresSimplesMinusculos;
    }
    let todosTiposCaractere = argumentos.length === 0;
    if (todosTiposCaractere || argumentos.indexOf('numero') !== -1) {
      caracteres += this.numeros;
    }
    if (todos_tipos_caractere || argumentos.indexOf('especial') !== -1) {
      caracteres += this.caracteresEspeciais;
    }

    return this.sortear(comprimento, caracteres);
  }

  embaralhar(vetor) {
    const resultado = [];
    for (let i = vetor.length - 1; i > 0; i--) {
      const j = this.gerarNumero(1, i);
      const temp = vetor[i];
      resultado[i] = vetor[j];
      resultado[j] = temp;
    }
    return resultado;
  }
}

module.exports = new GeradorDeSenha();
