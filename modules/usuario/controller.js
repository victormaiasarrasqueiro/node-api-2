const logger = require('../../config/logger');
const utils = require('../../util');
const consts = require('../../config/constants');
const services = require('../impl/factoryServices');
const validations = require('../impl/validations');


class UsuarioController {
  constructor() {
    this.service = services.usuario();
    this.validation = validations.usuario();
  }

  /**
   * Consultar dados do Usuario logado no site
   * @param HttpRequest req 
   * @param HttpResponse res 
   */
  consultarPorId(req, res) {
    const base = this;
    const idUsuario = req.params.idUsuario;
    // const idUsuarioToken = ***

    if (!idUsuario) {
      return res.status(consts.CODE_400).json(utils.invalidJson(null));
    }

    /* TODO depois que rotina de autenticacao tiver pronta
    if(idUsuario != idUsuarioToken){
      return res.status(consts.CODE_400).json(utils.invalidJson(null));
    }
    */
    base.service.consultarPorId(idUsuario).then((resultado) => {
      logger.logarDebug(`Executando consultarPorId [${resultado}]`, resultado);
      return res.status(consts.CODE_200).json(utils.successJson(resultado));
    }).catch((erro) => {
      logger.logarDebug(`Erro ao executar consultarPorId [${erro}]`, erro);
      res.status(consts.CODE_500).json(utils.errorJson(erro));
    });

    return null;
  }

  /**
   * Alterar Usuario logado no site
   * @param HttpRequest req 
   * @param HttpResponse res 
   */
  alterar(req, res) {
    const base = this;
    const idUsuario = req.params.idUsuario;
    const usuario = this.buildObject(req);
    // const idUsuarioToken = ***

    /* TODO depois que rotina de autenticacao tiver pronta
    if(idUsuario != idUsuarioToken){
      return res.status(consts.CODE_400).json(utils.invalidJson(null)); 
      // SEM PERMISSAO ALTERAR USUARIO QUE NAO E ELE
    }
    */

    if (base.validationValues(res, usuario)) {
      base.service.alterar(usuario).then((resultado) => {
        logger.logarDebug(`Executando alterar [${resultado}]`, resultado);
        return res.status(consts.CODE_200).json(utils.successJson(resultado));
      }).catch((erro) => {
        logger.logarDebug(`Erro ao executar alterar [${erro}]`, erro);
        res.status(consts.CODE_500).json(utils.errorJson(erro));
      });
    }
  }

  /**
   * Construindo o Objeto
   * @param HttpRequest req 
   */
  buildObject(req) {
    return {
      idUsuario: req.params.idUsuario,
      nmUsuario: req.body.nmUsuario,
      dsEmail: req.body.dsEmail,
      cdCpf: req.body.cdCpf,
      nuCelular: req.body.nuCelular,
    };
  }

  /**
   * 
   * @param HttpResponse res 
   * @param Any object 
   */
  validationValues(res, object) {
    const validation = this.validation.validate(object);
    if (validation.error) {
      return res.status(consts.CODE_400).json(validation.error.details);
    }
    return null;
  }
}// fim

module.exports = new UsuarioController();
