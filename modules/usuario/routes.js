const express = require('express');

const router = express.Router();
const controller = require('./controller');

router.get('/:idUsuario', controller.consultarPorId.bind(controller));

router.put('/:idUsuario', controller.alterar.bind(controller));

module.exports = router;
