const logger = require('../../config/logger');
const utils = require('../../util');
const consts = require('../../config/constants');
const services = require('../impl/factoryServices');
const validations = require('../impl/validations');


class LojaController {
  constructor() {
    this.service = services.loja();
    this.validation = validations.loja();
  }

  consultarPorId(req, res) {
    const base = this;

    if (base.validationValues(req).error) {
      return res.status(consts.CODE_400).json(utils.invalidJson(null));
    }

    const idLoja = req.body.idLoja || req.params.idLoja;

    base.service.consultarPorId(idLoja).then((resultado) => {
      logger.logarDebug(`Executando consulta loja [${resultado}]`, resultado);
      return res.status(consts.CODE_200).json(utils.successJson(resultado));
    }).catch((erro) => {
      logger.logarDebug(`Erro ao executar consulta loja [${erro}]`, erro);
      return res.status(consts.CODE_500).json(utils.errorJson(erro));
    });
    return null;
  }

  consultar(req, res) {
    const base = this;
    base.service.consultarTodos().then((resultado) => {
      logger.logarDebug(`Executando consulta loja [${resultado}]`, 'resultado');
      return res.status(consts.CODE_200).json(utils.successJson(resultado));
    }).catch((erro) => {
      logger.logarDebug(`Erro ao executar consulta loja [${erro}]`, erro);
      res.status(consts.CODE_500).json(utils.errorJson(erro));
    });
  }

  inserir(req, res) {
    const base = this;
    const loja = base.validationValues(req);
    if (!loja.error) {
      return res.status(consts.CODE_400).json(utils.invalidJson(null));
    }

    base.service.salvar(loja).then((resultado) => {
      logger.logarDebug(`Executando inserir loja [${resultado}]`, resultado);
      return res.status(consts.CODE_200).json(utils.successJson(resultado));
    }).catch((erro) => {
      logger.logarDebug(`Erro ao executar inserir loja [${erro}]`, erro);
      return res.status(consts.CODE_500).json(utils.errorJson(erro));
    });
    return null;
  }

  alterar(req, res) {
    const base = this;
    const loja = base.validationValues(req);
    if (!loja.error) {
      return res.status(consts.CODE_400).json(utils.invalidJson(null));
    }

    base.service.salvar(loja).then((resultado) => {
      logger.logarDebug(`Executando inserir loja [${resultado}]`, resultado);
      return res.status(consts.CODE_200).json(utils.successJson(resultado));
    }).catch((erro) => {
      logger.logarDebug(`Erro ao executar inserir loja [${erro}]`, erro);
      return res.status(consts.CODE_500).json(utils.errorJson(erro));
    });
    return null;
  }

  remover(req, res) {
    const base = this;
    const idLoja = req.body.idLoja || req.params.idLoja;
    if (base.validationValues(idLoja).error) {
      return res.status(consts.CODE_400).json(utils.invalidJson(null));
    }

    base.service.remover(idLoja).then((resultado) => {
      logger.logarDebug(`Executando consulta loja [${resultado}]`, resultado);
      return res.status(consts.CODE_200).json(utils.successJson(resultado));
    }).catch((erro) => {
      logger.logarDebug(`Erro ao executar consulta loja [${erro}]`, erro);
      return res.status(consts.CODE_500).json(utils.errorJson(erro));
    });
    return null;
  }

  validationValues(req) {
    if (req.params.idLoja) {
      return this.validation.validate({ idLoja: req.params.idLoja });
    }

    const loja = {
      nmLoja: req.body.nmLoja,
      nmRazaoSocial: req.body.nmRazaoSocial,
      vlCnpj: req.body.vlCnpj,
    };
    return this.validation.validate(loja);
  }
}

module.exports = new LojaController();
