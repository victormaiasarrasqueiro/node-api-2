const express = require('express');

const router = express.Router();
const controller = require('./controller');

router.get('/:idLoja', controller.consultarPorId.bind(controller));

router.get('/', controller.consultar.bind(controller));

router.post('/', controller.inserir.bind(controller));

router.put('/', controller.alterar.bind(controller));

router.delete('/:idLoja', controller.remover.bind(controller));

module.exports = router;
