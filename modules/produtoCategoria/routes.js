const express = require('express');

const router = express.Router();
const controller = require('./controller');

router.get('/:idProdutoCategoria', controller.consultarPorId.bind(controller));

router.get('/categoria/:idCategoria', controller.consultarProdutosPorCategoria.bind(controller));

router.post('/', controller.inserir.bind(controller));

router.delete('/:idProdutoCategoria', controller.remover.bind(controller));

module.exports = router;
