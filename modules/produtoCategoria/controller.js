const logger = require('../../config/logger');
const utils = require('../../util');
const consts = require('../../config/constants');
const services = require('../impl/factoryServices');
const validations = require('../impl/validations');


class ProdutoCategoriaController {
  constructor() {
    this.service = services.produtoCategoria();
    this.validation = validations.produtoCategoria();
  }

  consultarPorId(req, res) {
    const base = this;
    const validation = base.validationValues({ idProdutoCategoria: req.params.idProdutoCategoria });

    if (validation.error) {
      return res.status(consts.CODE_400).json(utils.invalidJson(validation.error.message));
    }

    const idProdutoCategoria = req.params.idProdutoCategoria;

    base.service.consultarPorId(idProdutoCategoria).then((resultado) => {
      logger.logarDebug(`Executando consulta ProdutoCategoriaController [${resultado}]`, resultado);
      return res.status(consts.CODE_200).json(utils.successJson(resultado));
    }).catch((erro) => {
      logger.logarDebug(`Erro ao executar consulta loja [${erro}]`, erro);
      return res.status(consts.CODE_500).json(utils.errorJson(erro));
    });
    return null;
  }

  consultarProdutosPorCategoria(req, res) {
    const base = this;
    const validation = base.validationValues({ idCategoria: req.params.idCategoria });

    if (validation.error) {
      return res.status(consts.CODE_400).json(utils.invalidJson(validation.error.message));
    }

    const idCategoria = req.params.idCategoria;

    base.service.consultarProdutosPorCategoria(idCategoria).then((resultado) => {
      logger.logarDebug(`Executando consulta consultarProdutosPorCategoria [${resultado}]`, resultado);
      return res.status(consts.CODE_200).json(utils.successJson(resultado));
    }).catch((erro) => {
      logger.logarDebug(`Erro ao executar consulta loja [${erro}]`, erro);
      res.status(consts.CODE_500).json(utils.errorJson(erro));
    });
    return null;
  }

  inserir(req, res) {
    const base = this;
    const loja = base.validationValues(req);
    if (!loja.error) {
      return res.status(consts.CODE_400).json(utils.invalidJson(null));
    }

    base.service.salvar(loja).then((resultado) => {
      logger.logarDebug(`Executando inserir loja [${resultado}]`, resultado);
      return res.status(consts.CODE_200).json(utils.successJson(resultado));
    }).catch((erro) => {
      logger.logarDebug(`Erro ao executar inserir loja [${erro}]`, erro);
      return res.status(consts.CODE_500).json(utils.errorJson(erro));
    });
    return null;
  }

  remover(req, res) {
    const base = this;
    const idProdutoCategoria = req.body.idProdutoCategoria || req.params.idProdutoCategoria;
    if (base.validationValues(idProdutoCategoria).error) {
      return res.status(consts.CODE_400).json(utils.invalidJson(null));
    }

    base.service.remover(idProdutoCategoria).then((resultado) => {
      logger.logarDebug(`Executando consulta produtoCategoria [${resultado}]`, resultado);
      return res.status(consts.CODE_200).json(utils.successJson(resultado));
    }).catch((erro) => {
      logger.logarDebug(`Erro ao executar consulta produtoCategoria [${erro}]`, erro);
      return res.status(consts.CODE_500).json(utils.errorJson(erro));
    });
    return null;
  }


  validationValues(obj) {
    const validation = this.validation.validate(obj);
    return validation;
  }

  buildObject(req) {
    const object = {
      idProdutoCategoria: req.body.idProdutoCategoria || req.params.idProdutoCategoria,
      idProduto: req.body.idProduto,
      idCategoria: req.body.idCategoria,
    };
    return object;
  }
}

module.exports = new ProdutoCategoriaController();
