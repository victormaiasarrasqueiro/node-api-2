const lojaService = require('../../modules/impl/service/lojaService');
const categoriaService = require('../../modules/impl/service/categoriaService');
const usuarioService = require('../../modules/impl/service/usuarioService');
const produtoCategoriaService = require('../../modules/impl/service/produtoCategoriaService');

class FactoryServices {
  constructor() {
    this.lojaService = lojaService;
    this.categoriaService = categoriaService;
    this.usuarioService = usuarioService;
    this.produtoCategoriaService = produtoCategoriaService;
  }

  loja() {
    return this.lojaService;
  }

  categoria() {
    return this.categoriaService;
  }

  usuario() {
    return this.usuarioService;
  }

  produtoCategoria() {
    return this.produtoCategoriaService;
  }
}
module.exports = new FactoryServices();
