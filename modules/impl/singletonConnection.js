// create a unique, global symbol name
// -----------------------------------

const MYSQL_CONNECTOR = Symbol.for('mysqlConnector');

// check if the global object has this symbol
// add it if it does not have the symbol, yet
// ------------------------------------------

const globalSymbols = Object.getOwnPropertySymbols(global);
const hasMysql = (globalSymbols.indexOf(MYSQL_CONNECTOR) > -1);

if (!hasMysql) {
  global[MYSQL_CONNECTOR] = {
    connection: require('../../config/db/mysql/mysqlConnector'),
  };
}

// define the singleton API
// ------------------------

const singleton = {};

Object.defineProperty(singleton, 'instance', {
  get() {
    return global[MYSQL_CONNECTOR];
  },
});

// ensure the API is never changed
// -------------------------------

Object.freeze(singleton);

// export the singleton API only
// -----------------------------

module.exports = singleton;
