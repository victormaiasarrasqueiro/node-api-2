const lojaService = require('../../modules/impl/validations/lojaValidations');
const categoriaValidation = require('../../modules/impl/validations/categoriaValidations');
const usuarioValidation = require('../../modules/impl/validations/usuarioValidations');
const produtoCategoria = require('../../modules/impl/validations/produtoCategoria');

class ValidationService {
  constructor() {
    this.lojaService = lojaService;
    this.categoriaValidation = categoriaValidation;
    this.usuarioValidation = usuarioValidation;
    this.produtoCategoriaValidation = produtoCategoria;
  }

  loja() {
    return this.lojaService;
  }

  categoria() {
    return this.categoriaValidation;
  }

  usuario() {
    return this.usuarioValidation;
  }

  produtoCategoria() {
    return this.produtoCategoriaValidation;
  }
}
module.exports = new ValidationService();
