const model = require('../../../models');
const logger = require('../../../config/logger');

class LojaDao {
  constructor() {
    this.model = model.loja;
  }

  findById(pIdLoja) {
    try {
      logger.logarDebug('Executando findById', 'LojaDao');
      return this.model.findOne({
        where: {
          ID_LOJA: pIdLoja,
        },
        include: [{
          model: model.lojaEndereco,
          as: 'lojaEndereco',
        },
        {
          model: model.lojaContato,
          as: 'lojaContato',
        }],
      });
    } catch (err) {
      logger.logarDebug('Erro ao executar findById', err);
    }
    return null;
  }

  findAll() {
    try {
      logger.logarDebug('Executando findAll', 'LojaDao');
      return this.model.findAll({
        include: [{
          model: model.lojaEndereco,
          as: 'lojaEndereco',
        },
        {
          model: model.lojaContato,
          as: 'lojaContato',
        }],
      });
    } catch (err) {
      logger.logarDebug('Erro ao executar findAll', err);
    }
    return null;
  }

  save(pLoja) {
    try {
      logger.logarDebug('Executando save', 'LojaDao');
      return this.model.create({
        NM_FANTASIA: pLoja.nmFantasia,
        NM_RAZAO_SOCIAL: pLoja.nmRazaoSocial,
        CD_CNPJ: pLoja.cdCnpj,
        IMAGEM_ID: pLoja.imagemId,
        DT_INCLUSAO: Date.now(),

      });
    } catch (err) {
      logger.logarDebug('Erro ao executar save', err);
    }
    return null;
  }

  delete(pIdLoja) {
    try {
      logger.logarDebug('Executando delete', 'LojaDao');
      return this.model.destroy({
        where: {
          ID_LOJA: pIdLoja,
        },
      });
    } catch (err) {
      logger.logarDebug('Erro ao executar delete', err);
    }
    return null;
  }
}

module.exports = new LojaDao();
