const model = require('../../../models');
const logger = require('../../../config/logger');

class ProdutoCategoriaDao {
  constructor() {
    this.model = model.produtoCategoria;
  }

  consultarProdutosPorCategoria(idCategoria) {
    try {
      return this.model.findAll({
        where: {
          ID_CATEGORIA: idCategoria,
        },
        include: [{
          model: model.categoria,
        }],
      });
    } catch (err) {
      logger.logarDebug('Erro ao executar consultar ProdutosPorCategoria', err);
    }
    return null;
  }

  findById(pIdProdutoCategoria) {
    try {
      logger.logarDebug('Executando findById', 'ProdutoCategoriaDao');
      return this.model.findOne({
        where: {
          ID_PRODUTO_CATEGORIA: pIdProdutoCategoria,
        },
      });
    } catch (err) {
      logger.logarDebug('Erro ao executar findById', err);
    }
    return null;
  }

  save(pObject) {
    try {
      logger.logarDebug('Executando save', 'ProdutoCategoriaDao');
      return this.model.create({
        ID_CATEGORIA: pObject.nmRazaoSocial,
        ID_PRODUTO: pObject.vlCnpj,
        DT_INCLUSAO: Date.now(),
      });
    } catch (err) {
      logger.logarDebug('Erro ao executar save', err);
    }
    return null;
  }

  delete(pId) {
    try {
      logger.logarDebug('Executando delete', 'ProdutoCategoriaDao');
      return this.model.destroy({
        where: {
          ID_PRODUTO_CATEGORIA: pId,
        },
      });
    } catch (err) {
      logger.logarDebug('Erro ao executar delete', err);
    }
    return null;
  }
}

module.exports = new ProdutoCategoriaDao();
