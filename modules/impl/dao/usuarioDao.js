const usuarioModel = require('../../../models');
const logger = require('../../../config/logger');

class UsuarioDao {
  constructor() {
    this.Usuario = usuarioModel.usuario;
  }

  consultarPorId(idUsuario) {
    try {
      logger.logarDebug('Executando consultarPorId', 'UsuarioDao');
      return this.Usuario.findOne({
        where: {
          IS_ATIVO: 1,
          ID_USUARIO: idUsuario,
        },
      });
    } catch (err) {
      logger.logarDebug('Erro ao executar consultarPorId', err);
    }
    return null;
  }

  alterar(pObject) {
    try {
      logger.logarDebug('Executando alterar', 'UsuarioDao');
      return this.Usuario.update(
        {
          NM_USUARIO: pObject.nmUsuario,
          DS_EMAIL: pObject.dsEmail,
          CD_CPF: pObject.cdCpf,
          NU_CELULAR: pObject.nuCelular,
        },
        { where: { ID_USUARIO: pObject.idUsuario } });
    } catch (err) {
      logger.logarDebug('Erro ao executar alterar', err);
    }
    return null;
  }
}// fim

module.exports = new UsuarioDao();
