const model = require('../../../models');
const logger = require('../../../config/logger');

class CategoriaDao {
  constructor() {
    this.model = model.categoria;
  }

  /**
   * Recuperar as categorias que nao possuem pai. ( idPai = null )
   * @param HttpRequest req 
   * @param HttpResponse res 
   */
  consultarSemPai() {
    try {
      logger.logarDebug('Executando findAll', 'CategoriaDao');
      return this.model.findAll({
        attributes: ['ID_CATEGORIA', 'NM_CATEGORIA'],
        where: {
          IS_ATIVO: 1,
          ID_CATEGORIA_PAI: null,
        },
        include: [{
          model: model.produto,
        }],
      });
    } catch (err) {
      logger.logarDebug('Erro ao executar findAll', err);
    }
    return null;
  }

  /**
   * Consultar Categorias de um mesmo pai
   * @param HttpRequest req 
   * @param HttpResponse res 
   */
  consultarFilhos(idCategoriaPai) {
    try {
      logger.logarDebug('Executando consultarFilhos', 'CategoriaDao');
      return this.model.findAll({
        attributes: ['ID_CATEGORIA', 'ID_CATEGORIA_PAI', 'NM_CATEGORIA'],
        where: {
          IS_ATIVO: 1,
         // ID_CATEGORIA_PAI: idCategoriaPai,
        },
        include: [{
          model: model.produto,
        }],
      });
    } catch (err) {
      logger.logarDebug('Erro ao executar consultarFilhos', err);
    }
    return null;
  }
}// fim

module.exports = new CategoriaDao();
