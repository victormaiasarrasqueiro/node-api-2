const lojaDao = require('../../modules/impl/dao/lojaDao');
const categoriaDao = require('../../modules/impl/dao/categoriaDao');
const usuarioDao = require('../../modules/impl/dao/usuarioDao');
const produtoCategoriaDao = require('../../modules/impl/dao/produtoCategoriaDao');

class DaoFactory {
  constructor() {
    this.lojaDao = lojaDao;
    this.categoriaDao = categoriaDao;
    this.usuarioDao = usuarioDao;
    this.produtoCategoriaDao = produtoCategoriaDao;
  }

  loja() {
    return this.lojaDao;
  }

  categoria() {
    return this.categoriaDao;
  }

  usuario() {
    return this.usuarioDao;
  }

  produtoCategoria() {
    return this.produtoCategoriaDao;
  }
}
module.exports = new DaoFactory();
