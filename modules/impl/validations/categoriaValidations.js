const Joi = require('joi');

class CategoriaValidate {
  constructor() {
    this.schema = Joi.object().keys({
      id: Joi.number().integer().min(1).max(9999999),
    });
  }


  validate(pObject) {
    return Joi.validate(pObject, this.schema);
  }
}

module.exports = new CategoriaValidate();
