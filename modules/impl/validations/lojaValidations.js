const Joi = require('joi');

class LojaValidate {
  constructor() {
    this.schema = Joi.object().keys({
      idLoja: Joi.number().integer().min(1).max(9999999),
      nomeFantasia: Joi.string().alphanum().min(1).max(100)
        .required(),
      nomeRazaoSocial: Joi.string().alphanum().min(1).max(100)
        .required(),
      cnpj: Joi.string().regex(/^[0-9]{15}$/),
    });
  }


  validate(pObject) {
    return Joi.validate(pObject, this.schema);
  }
}

module.exports = new LojaValidate();
