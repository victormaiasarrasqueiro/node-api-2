const Joi = require('joi');

class ProdutoCategoriaValidate {
  constructor() {
    this.schema = Joi.object().keys({
      idProdutoCategoria: Joi.number().integer().min(1).max(9999999),
      idLoja: Joi.number().integer().min(1).max(9999999),
      idCategoria: Joi.number().integer().min(1).max(9999999),
    });
  }


  validate(pObject) {
    return Joi.validate(pObject, this.schema);
  }
}

module.exports = new ProdutoCategoriaValidate();
