const Joi = require('joi');

class UsuarioValidate {
  constructor() {
    this.schema = Joi.object().keys({
      idUsuario: Joi.number().integer().min(1).required(),
      nmUsuario: Joi.string().min(5).max(45).required(),
      dsEmail: Joi.string().email().max(60),
      cdCpf: Joi.string().alphanum().min(5).max(15)
        .required(),
      nuCelular: Joi.string().alphanum().min(5).max(15)
        .required(),
    });
  }

  validate(pObject) {
    return Joi.validate(pObject, this.schema);
  }
}

module.exports = new UsuarioValidate();
