const logger = require('../../../config/logger');
const util = require('../../../util');
const dao = require('../../impl/daoFactory');

class ProdutoCategoriaService {
  constructor() {
    this.dao = dao.produtoCategoria();
  }

  consultarPorId(pIdLoja) {
    logger.logarInfo('Iniciando a consulta', 'teste');
    const base = this;

    return new Promise((resolve, reject) => {
      base.dao.findById(pIdLoja)
        .then((resposta) => {
          logger.logarInfo(`Retorno ProdutoCategoriaService consultarPorId [${resposta}]`, resposta);
          if (resposta.dataValues) {
            resolve(util.camelDeepSequelize(resposta.dataValues));
          } else {
            resolve(null);
          }
        })
        .catch((erro) => {
          logger.logarInfo(`Erro ao executar ProdutoCategoriaService consultarPorId [${erro}]`, erro);
          reject(erro);
        });
    });
  }

  consultarProdutosPorCategoria(idCategoria) {
    logger.logarInfo('Iniciando consultarProdutosPorCategoria', idCategoria);
    const base = this;

    return new Promise((resolve, reject) => {
      base.dao.consultarProdutosPorCategoria(idCategoria)
        .then((resposta) => {
          logger.logarInfo(`Retorno ProdutoCategoriaService consultarProdutosPorCategoria [${resposta}]`, resposta);
          if (resposta.length > 0) {
            resolve(util.camelDeepSequelize(resposta[0].dataValues));
          } else {
            resolve(null);
          }
        })
        .catch((erro) => {
          logger.logarInfo(`Erro ao executar ProdutoCategoriaService consultarTodos [${erro}]`, erro);
          reject(erro);
        });
    });
  }

  salvar(pObject) {
    logger.logarInfo('Iniciando salvar', pObject);
    const base = this;

    return new Promise((resolve, reject) => {
      base.dao.save(pObject)
        .then((resposta) => {
          logger.logarInfo(`Retorno ProdutoCategoriaService salvar [${resposta}]`, resposta);
          resolve(resposta);
        })
        .catch((erro) => {
          logger.logarInfo(`Erro ao executar ProdutoCategoriaService salvar [${erro}]`, erro);
          reject(erro);
        });
    });
  }

  remover(pId) {
    logger.logarInfo('Iniciando  remover', pId);
    const base = this;

    return new Promise((resolve, reject) => {
      base.dao.delete(pId)
        .then((resposta) => {
          logger.logarInfo(`Retorno ProdutoCategoriaService remover [${resposta}]`, resposta);
          resolve(resposta);
        })
        .catch((erro) => {
          logger.logarInfo(`Erro ao executar ProdutoCategoriaService remover [${erro}]`, erro);
          reject(erro);
        });
    });
  }
}

module.exports = new ProdutoCategoriaService();
