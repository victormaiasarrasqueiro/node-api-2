const logger = require('../../../config/logger');
const util = require('../../../util');
const dao = require('../../impl/daoFactory');

class CategoriaService {
  constructor() {
    this.dao = dao.categoria();
  }

  /**
   * Recuperar as categorias que nao possuem pai. ( idPai = null )
   * @param HttpRequest req 
   * @param HttpResponse res 
   */
  consultarSemPai() {
    logger.logarInfo('Iniciando consultarSemPai', 'CategoriaService');
    const base = this;
    return new Promise((resolve, reject) => {
      base.dao.consultarSemPai()
        .then((resposta) => {
          logger.logarInfo(`Retorno CategoriaService consultarSemPai [${resposta}]`, resposta);
          if (resposta.length > 0) {
            resolve(util.camelDeepSequelize(resposta));
          } else {
            resolve(null);
          }
        })
        .catch((erro) => {
          logger.logarInfo(`Erro ao executar CategoriaService consultarSemPai [${erro}]`, erro);
          reject(erro);
        });
    });
  }

  /**
   * Consultar Categorias de um mesmo pai
   * @param HttpRequest req 
   * @param HttpResponse res 
   */
  consultarFilhos(idCategoriaPai) {
    logger.logarInfo('Iniciando consultarFilhos', 'CategoriaService');
    const base = this;
    return new Promise((resolve, reject) => {
      base.dao.consultarFilhos(idCategoriaPai)
        .then((resposta) => {
          logger.logarInfo(`Retorno CategoriaService consultarFilhos [${resposta}]`, resposta);
          if (resposta.length > 0) {
            resolve(util.camelDeepSequelize(resposta));
          } else {
            resolve(null);
          }
        })
        .catch((erro) => {
          logger.logarInfo(`Erro ao executar CategoriaService consultarFilhos [${erro}]`, erro);
          reject(erro);
        });
    });
  }
}// fim

module.exports = new CategoriaService();
