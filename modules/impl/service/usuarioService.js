const logger = require('../../../config/logger');
const util = require('../../../util');
const dao = require('../../impl/daoFactory');

class UsuarioService {
  constructor() {
    this.dao = dao.usuario();
  }

  consultarPorId(idUsuario) {
    logger.logarInfo('Iniciando consultarPorId', 'UsuarioService');
    const base = this;
    return new Promise((resolve, reject) => {
      base.dao.consultarPorId(idUsuario)
        .then((resposta) => {
          logger.logarInfo(`Retorno UsuarioService consultarPorId [${resposta}]`, resposta);
          if (resposta) {
            resolve(util.camelDeepSequelize(resposta));
          } else {
            resolve(null);
          }
        })
        .catch((erro) => {
          logger.logarInfo(`Erro ao executar UsuarioService consultarPorId [${erro}]`, erro);
          reject(erro);
        });
    });
  }

  alterar(usuario) {
    logger.logarInfo('Iniciando alterar', 'UsuarioService');
    const base = this;
    return new Promise((resolve, reject) => {
      base.dao.alterar(usuario)
        .then((resposta) => {
          logger.logarInfo(`Retorno UsuarioService alterar [${resposta}]`, resposta);
          resolve(resposta);
        })
        .catch((erro) => {
          logger.logarInfo(`Erro ao executar UsuarioService alterar [${erro}]`, erro);
          reject(erro);
        });
    });
  }
}// fim

module.exports = new UsuarioService();
