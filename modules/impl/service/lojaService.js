const logger = require('../../../config/logger');
const util = require('../../../util');
const dao = require('../../impl/daoFactory');

class LojaService {
  constructor() {
    this.dao = dao.loja();
  }

  consultarPorId(pId) {
    logger.logarInfo('Iniciando a consulta', 'teste');
    const base = this;

    return new Promise((resolve, reject) => {
      base.dao.findById(pId)
        .then((resposta) => {
          logger.logarInfo(`Retorno LojaService consultarPorId [${resposta}]`, resposta);
          if (resposta.dataValues) {
            resolve(util.camelDeepSequelize(resposta.dataValues));
          } else {
            resolve(null);
          }
        })
        .catch((erro) => {
          logger.logarInfo(`Erro ao executar LojaService consultarPorId [${erro}]`, erro);
          reject(erro);
        });
    });
  }

  consultarTodos() {
    logger.logarInfo('Iniciando consultarTodos', 'LojaService');
    const base = this;

    return new Promise((resolve, reject) => {
      base.dao.findAll()
        .then((resposta) => {
          logger.logarInfo(`Retorno LojaService consultarTodos [${resposta}]`, 'consultarTodos');
          if (resposta.length > 0) {
            resolve(util.camelDeepSequelize(resposta[0].dataValues));
          } else {
            resolve(null);
          }
        })
        .catch((erro) => {
          logger.logarInfo(`Erro ao executar LojaService consultarTodos [${erro}]`, erro);
          reject(erro);
        });
    });
  }

  salvar(pObject) {
    logger.logarInfo('Iniciando salvar', pObject);
    const base = this;

    return new Promise((resolve, reject) => {
      base.dao.save(pObject)
        .then((resposta) => {
          logger.logarInfo(`Retorno LojaService salvar [${resposta}]`, resposta);
          resolve(resposta);
        })
        .catch((erro) => {
          logger.logarInfo(`Erro ao executar LojaService salvar [${erro}]`, erro);
          reject(erro);
        });
    });
  }

  remover(pIdLoja) {
    logger.logarInfo('Iniciando  remover', pIdLoja);
    const base = this;

    return new Promise((resolve, reject) => {
      base.dao.delete(pIdLoja)
        .then((resposta) => {
          logger.logarInfo(`Retorno LojaService remover [${resposta}]`, resposta);
          resolve(resposta);
        })
        .catch((erro) => {
          logger.logarInfo(`Erro ao executar LojaService remover [${erro}]`, erro);
          reject(erro);
        });
    });
  }
}

module.exports = new LojaService();
