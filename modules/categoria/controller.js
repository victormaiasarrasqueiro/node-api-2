const logger = require('../../config/logger');
const utils = require('../../util');
const consts = require('../../config/constants');
const services = require('../impl/factoryServices');
const validations = require('../impl/validations');


class CategoriaController {
  constructor() {
    this.service = services.categoria();
    this.validation = validations.categoria();
  }

  /**
   * Recuperar as categorias que nao possuem pai. ( idPai = null )
   * @param HttpRequest req 
   * @param HttpResponse res 
   */
  consultarSemPai(req, res) {
    const base = this;
    base.service.consultarSemPai().then((resultado) => {
      logger.logarDebug(`Executando consulta categoria [${resultado}]`, resultado);
      return res.status(consts.CODE_200).json(utils.successJson(resultado));
    }).catch((erro) => {
      logger.logarDebug(`Erro ao executar consulta categoria [${erro}]`, erro);
      res.status(consts.CODE_500).json(utils.errorJson(erro));
    });
  }

  /**
   * Consultar Categorias de um mesmo pai
   * @param HttpRequest req 
   * @param HttpResponse res 
   */
  consultarFilhos(req, res) {
    const base = this;
    const idCategoriaPai = req.params.idPai;

    if (!idCategoriaPai) {
      return res.status(consts.CODE_400).json(utils.invalidJson(null));
    }

    base.service.consultarFilhos(idCategoriaPai).then((resultado) => {
      logger.logarDebug(`Executando consulta categoria [${resultado}]`, resultado);
      return res.status(consts.CODE_200).json(utils.successJson(resultado));
    }).catch((erro) => {
      logger.logarDebug(`Erro ao executar consulta categoria [${erro}]`, erro);
      res.status(consts.CODE_500).json(utils.errorJson(erro));
    });
    return null;
  }
}// fim

module.exports = new CategoriaController();
