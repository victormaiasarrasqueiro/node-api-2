const express = require('express');

const router = express.Router();
const controller = require('./controller');

router.get('/', controller.consultarSemPai.bind(controller));

router.get('/:idPai', controller.consultarFilhos.bind(controller));

module.exports = router;
