// Mock
process.env.DEBUG = 'swagger:middleware';

require('./config/helpers');
const config = require('./config');
const logger = require('./config/logger');

const express = require('express');
const middleware = require('swagger-express-middleware');
const path = require('path');

const app = express();
const router = express.Router();
const MemoryDataStore = middleware.MemoryDataStore;
const Resource = middleware.Resource;

middleware(path.join(__dirname, 'config/swagger/swagger.yaml'), app, (err, middleware) => {
  app.use(
    middleware.metadata(),
    middleware.CORS(),
    middleware.files(),
    middleware.parseRequest(),
    middleware.validateRequest(),
  );

  const db = new MemoryDataStore();
  let resources = [];

  Helper.forEachModuleFileType('mock', (entry, file_name) => {
    let module_mock = require(file_name);
    router.use(`/${entry}`, module_mock.router);
    resources = resources.concat(module_mock.db.map((registry) => {
      return new Resource(`/${entry}${registry.endpoint}`,registry.response);
    }));
  });

  app.use('/', router);
  db.save(resources);
  app.use(middleware.mock(db));

  if (err) {
    logger.logarErro('Aconteceu um erro: ', err);
  }

  app.listen(config.mock.port, () => {
    logger.logarInfo(`Servidor mock rodando na porta: ${config.mock.port}`);
  });
});

// Docs
const docs_app = express();
const docs_router = express.Router();

docs_router.use('/', express.static(`${__dirname}/docs`));
docs_app.use(docs_router);

docs_app.listen(config.docs.port, () => {
  logger.logarInfo(`Documentação rodando na porta ${config.docs.port}`);
});
