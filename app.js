require('./config/helpers');
const config = require('./config');
const app = require('./config/express');
const logger = require('./config/logger');
const singletonConnection = require('./modules/impl/singletonConnection');
const constantes = require('./config/constants');

process.on('uncaughtException', (err) => {
  logger.logarErro('uncaughtException', err);
});


app.use((err, req, res) => {
  res.status(err.status || 500).json(err.message);
});


module.exports = app.listen(config.port, () => {
  app.set(constantes.CONNECTION, singletonConnection.instance.connection.pool);

  const con = app.get('singletonConnection');

  con.authenticate()
    .then(() => {
      logger.logarInfo('Connection has been established successfully', 'App.js');
    })
    .catch((err) => {
      logger.logarInfo(`Unable to connect to the database: ${err}`, err);
    });
});

module.exports.app = app;
