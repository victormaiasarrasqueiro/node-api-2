

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('TB_PROPRIEDADE_VALOR_DEFAULT', {
    DS_VALOR: {
      type: Sequelize.STRING(50),
      allowNull: false,
    },
    ID_PROPRIEDADE: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      references: {
        model: 'TB_PROPRIEDADE',
        key: 'ID_PROPRIEDADE',
      },
    },
    IS_ATIVO: {
      type: Sequelize.INTEGER(4),
      allowNull: false,
      defaultValue: '1',
    },
    DT_INCLUSAO: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.NOW,
    },
  }, {
    tableName: 'TB_PROPRIEDADE_VALOR_DEFAULT',
  }),
  down: (queryInterface, Sequelize) => queryInterface.dropTable('TB_PROPRIEDADE_VALOR_DEFAULT'),
};
