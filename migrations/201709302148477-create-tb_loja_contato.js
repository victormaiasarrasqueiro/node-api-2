

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('TB_LOJA_CONTATO', {
    ID_LOJA_CONTATO: {
      type: Sequelize.INTEGER(2),
      allowNull: false,
      primaryKey: true,
    },
    TP_MEIO_CONTATO: {
      type: Sequelize.INTEGER(2),
      allowNull: false,
      defaultValue: '1',
    },
    DS_VALOR: {
      type: Sequelize.STRING(45),
      allowNull: false,
    },
    IS_ATIVO: {
      type: Sequelize.INTEGER(1),
      allowNull: false,
      defaultValue: '1',
    },
    DT_INCLUSAO: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.NOW,
    },
    ID_LOJA: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'TB_LOJA',
        key: 'ID_LOJA',
      },
    },
  }, {
    tableName: 'TB_LOJA_CONTATO',
  }),
  down: (queryInterface, Sequelize) => queryInterface.dropTable('TB_LOJA_CONTATO'),
};
