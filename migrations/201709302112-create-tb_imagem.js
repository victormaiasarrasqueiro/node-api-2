

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('TB_IMAGEM', {
    ID_IMAGEM: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    TP_IMAGEM: {
      type: Sequelize.STRING(5),
      allowNull: false,
      defaultValue: 'jpg',
    },
    ID_BUCKET: {
      type: Sequelize.INTEGER(2),
      allowNull: false,
      defaultValue: '1',
    },
  }, {
    tableName: 'TB_IMAGEM',
  }),
  down: (queryInterface, Sequelize) => queryInterface.dropTable('TB_IMAGEM'),
};
