

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('TB_CATEGORIA', {
    ID_CATEGORIA: {
      type: Sequelize.INTEGER(5),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    ID_CATEGORIA_PAI: {
      type: Sequelize.INTEGER(5),
      allowNull: true,
      references: {
        model: 'TB_CATEGORIA',
        key: 'ID_CATEGORIA',
      },
    },
    NM_CATEGORIA: {
      type: Sequelize.STRING(100),
      allowNull: false,
    },
    IS_ATIVO: {
      type: Sequelize.INTEGER(1),
      allowNull: false,
      defaultValue: '0',
    },
    DT_INCLUSAO: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.NOW,
    },
  }, {
    tableName: 'TB_CATEGORIA',
  }),
  down: (queryInterface, Sequelize) => queryInterface.dropTable('TB_CATEGORIA'),
};
