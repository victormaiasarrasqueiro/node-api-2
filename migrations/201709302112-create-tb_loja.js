

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('TB_LOJA', {
    ID_LOJA: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    NM_FANTASIA: {
      type: Sequelize.STRING(100),
      allowNull: false,
    },
    NM_RAZAO_SOCIAL: {
      type: Sequelize.STRING(100),
      allowNull: true,
    },
    CD_CNPJ: {
      type: Sequelize.STRING(15),
      allowNull: true,
    },
    IMAGEM_ID: {
      type: Sequelize.BIGINT,
      allowNull: true,
      references: {
        model: 'TB_IMAGEM',
        key: 'ID_IMAGEM',
      },
    },
    DT_INCLUSAO: {
      type: Sequelize.DATE,
      allowNull: true,
      defaultValue: Sequelize.NOW,
    },
    IS_ATIVO: {
      type: Sequelize.INTEGER(4),
      allowNull: true,
      defaultValue: '0',
    },
  }, {
    tableName: 'TB_LOJA',
  }),
  down: (queryInterface, Sequelize) => queryInterface.dropTable('TB_LOJA'),
};
