

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('TB_PRODUTO_PROPRIEDADE_VALOR', {
    ID_PRODUTO_PROPRIEDADE_VALOR: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    ID_PRODUTO: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'TB_PRODUTO',
        key: 'ID_PRODUTO',
      },
    },
    ID_PROPRIEDADE: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'TB_PROPRIEDADE',
        key: 'ID_PROPRIEDADE',
      },
    },
    DS_VALOR: {
      type: Sequelize.STRING(50),
      allowNull: false,
    },
    DT_INCLUSAO: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.NOW,
    },
  }, {
    tableName: 'TB_PRODUTO_PROPRIEDADE_VALOR',
  }),
  down: (queryInterface, Sequelize) => queryInterface.dropTable('TB_PRODUTO_PROPRIEDADE_VALOR'),
};
