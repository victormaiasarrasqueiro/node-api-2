

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('TB_LOJA_PRODUTO', {
    ID_LOJA: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'TB_LOJA',
        key: 'ID_LOJA',
      },
    },
    ID_PRODUTO: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'TB_PRODUTO',
        key: 'ID_PRODUTO',
      },
    },
    DS_PRECO: {
      type: Sequelize.FLOAT,
      allowNull: false,
    },
    IS_ATIVO: {
      type: Sequelize.INTEGER(4),
      allowNull: false,
      defaultValue: '1',
    },
    DT_INCLUSAO: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.NOW,
    },
  }, {
    tableName: 'TB_LOJA_PRODUTO',
  }),
  down: (queryInterface, Sequelize) => queryInterface.dropTable('TB_LOJA_PRODUTO'),
};
