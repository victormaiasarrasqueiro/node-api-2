

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('TB_PRODUTO_CATEGORIA', {
    ID_PRODUTO_CATEGORIA: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    ID_PRODUTO: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'TB_PRODUTO',
        key: 'ID_PRODUTO',
      },
    },
    ID_CATEGORIA: {
      type: Sequelize.INTEGER(5),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'TB_CATEGORIA',
        key: 'ID_CATEGORIA',
      },
    },
    DT_INCLUSAO: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.NOW,
    },
  }, {
    tableName: 'TB_PRODUTO_CATEGORIA',
  }),
  down: (queryInterface, Sequelize) => queryInterface.dropTable('TB_PRODUTO_CATEGORIA'),
};
