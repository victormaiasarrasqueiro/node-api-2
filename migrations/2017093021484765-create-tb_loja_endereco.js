

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('TB_LOJA_ENDERECO', {
    ID_LOJA_ENDERECO: {
      type: Sequelize.INTEGER(2),
      allowNull: false,
      primaryKey: true,
    },
    NU_CEP: {
      type: Sequelize.STRING(10),
      allowNull: false,
    },
    DS_ENDERECO: {
      type: Sequelize.STRING(25),
      allowNull: false,
    },
    NU_LOGRADOURO: {
      type: Sequelize.STRING(5),
      allowNull: false,
    },
    NM_CIDADE: {
      type: Sequelize.STRING(25),
      allowNull: false,
    },
    NM_ESTADO: {
      type: Sequelize.STRING(5),
      allowNull: false,
    },
    NM_PAIS: {
      type: Sequelize.STRING(15),
      allowNull: false,
    },
    CD_X: {
      type: Sequelize.DOUBLE,
      allowNull: false,
    },
    CD_Y: {
      type: Sequelize.DOUBLE,
      allowNull: false,
    },
    NM_BAIRRO: {
      type: Sequelize.STRING(15),
      allowNull: false,
    },
    DS_COMPLEMENTO: {
      type: Sequelize.STRING(15),
      allowNull: true,
    },
    IS_ATIVO: {
      type: Sequelize.INTEGER(4),
      allowNull: false,
      defaultValue: '1',
    },
    DT_INCLUSAO: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.NOW,
    },
    ID_LOJA: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'TB_LOJA',
        key: 'ID_LOJA',
      },
    },
  }, {
    tableName: 'TB_LOJA_ENDERECO',
  }),
  down: (queryInterface, Sequelize) => queryInterface.dropTable('TB_LOJA_ENDERECO'),
};
