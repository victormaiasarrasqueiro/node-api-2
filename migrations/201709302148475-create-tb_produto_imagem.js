

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('TB_PRODUTO_IMAGEM', {
    ID_IMAGEM: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'TB_IMAGEM',
        key: 'ID_IMAGEM',
      },
    },
    ID_PRODUTO: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'TB_PRODUTO',
        key: 'ID_PRODUTO',
      },
    },
    IS_IMAGEM_PRINCIPAL: {
      type: Sequelize.INTEGER(4),
      allowNull: false,
      defaultValue: '0',
    },
    DT_INCLUSAO: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.NOW,
    },
    IS_ATIVO: {
      type: Sequelize.INTEGER(4),
      allowNull: false,
      defaultValue: '1',
    },
  }, {
    tableName: 'TB_PRODUTO_IMAGEM',
  }),
  down: (queryInterface, Sequelize) => queryInterface.dropTable('TB_PRODUTO_IMAGEM'),
};
