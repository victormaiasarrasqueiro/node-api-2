

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('TB_PRODUTO', {
    ID_PRODUTO: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    NM_PRODUTO: {
      type: Sequelize.STRING(45),
      allowNull: false,
    },
    DS_PRODUTO_HTML: {
      type: Sequelize.TEXT,
      allowNull: false,
    },
    IS_ATIVO: {
      type: Sequelize.INTEGER(4),
      allowNull: false,
      defaultValue: '1',
    },
    DT_INCLUSAO: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.NOW,
    },
  }, {
    tableName: 'TB_PRODUTO',
  }),
  down: (queryInterface, Sequelize) => queryInterface.dropTable('TB_PRODUTO'),
};
