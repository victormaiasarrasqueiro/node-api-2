

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('TB_USUARIO_HISTORICO_LOGIN', {
    DT_LOGIN: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.NOW,
    },
    NU_IP: {
      type: Sequelize.STRING(12),
      allowNull: true,
    },
    ID_USUARIO: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      references: {
        model: 'TB_USUARIO',
        key: 'ID_USUARIO',
      },
    },
  }, {
    tableName: 'TB_USUARIO_HISTORICO_LOGIN',
  }),
  down: (queryInterface, Sequelize) => queryInterface.dropTable('TB_USUARIO_HISTORICO_LOGIN'),
};
