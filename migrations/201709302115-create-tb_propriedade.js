

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('TB_PROPRIEDADE', {
    ID_PROPRIEDADE: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    NM_PROPRIEDADE: {
      type: Sequelize.STRING(45),
      allowNull: false,
    },
    CD_PROPRIEDADE: {
      type: Sequelize.STRING(100),
      allowNull: false,
    },
    DT_INCLUSAO: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.DATE,
    },
    IS_ATIVO: {
      type: Sequelize.INTEGER(4),
      allowNull: false,
      defaultValue: '1',
    },
  }, {
    tableName: 'TB_PROPRIEDADE',
  }),
  down: (queryInterface, Sequelize) => queryInterface.dropTable('TB_PROPRIEDADE'),
};
