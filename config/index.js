const fs = require('fs');

require('../config/helpers');

let config = {
  path: {
    root: Helper.rootPath(),
    log: `${Helper.rootPath()}/error.log`,
    modules: `${Helper.rootPath()}/modules`,
  },
};

const environment = process.env.NODE_ENV || 'default';
const path = `${config.path.root}/config/env/${environment}`;
if (!fs.existsSync(path)) {
  const env_config = require(path);
  if (typeof env_config === 'object') {
    config = Object.assign(config, env_config);
  }
}

module.exports = config;
