const pretty = require('pretty-format');
const bunyan = require('bunyan');
const os = require('os');

class Log {
  constructor() {
    this.hostname = os.hostname() || 'localhost';

    this.logger = bunyan.createLogger({
      name: 'logger',
      streams: [
        {
          level: 'info',
          stream: process.stdout,
        },
        {
        // O log de erro tem a extensão .txt para não ser capturado pelo filtro
        // do filebeat que joga os logs para o elastic search
        // Colocamos ele como TXT pois o log de DEBUG já contem os erros.
          level: 'error',
          path: `logs/${this.hostname}.error.txt`,
          type: 'rotating-file',
          period: '1d',
          count: 15,
        },
        {
          level: 'debug',
          path: `logs/${this.hostname}.debug.log`,
          type: 'rotating-file',
          period: '1d',
          count: 15,
        },
      ],
    });
  }

  logarInfo(mensagem, objeto) {
    const log = this.montarLog(mensagem, objeto);
    this.logger.info(log);
    return log;
  }

  logarDebug(mensagem, objeto) {
    const log = this.montarLog(mensagem, objeto);
    this.logger.debug(log);
    return log;
  }

  logarErro(mensagem, objeto) {
    const log = this.montarLog(mensagem, objeto);
    this.logger.error(log);
    return log;
  }

  montarLog(mensagem, objeto) {
  // transformando o objeto em string para gravar no log
    const aux = pretty(objeto);

    const linha = {
      log: {
        mensagem,
        aux,
      },
    };

    if (objeto && objeto.tempoResposta) {
      linha.log.campos = {
        obj: objeto,
      };
    }

    return linha;
  }
}

module.exports = new Log();
