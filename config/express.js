const express = require('express');
const bodyParser = require('body-parser');
const compression = require('compression');
const helmet = require('helmet');
const cors = require('cors');
const requestIp = require('request-ip');

const app = express();

app.use(helmet());

const corsOptions = {
  origin: ['http://localhost'],
  methods: ['GET', 'POST', 'PUT', 'DELETE'],
  allowedHeaders: ['Content-Type', 'Authorization'],
};
app.use(cors(corsOptions));
app.use(bodyParser.urlencoded({ limit: '2mb', extended: false }));
app.use(bodyParser.json({ limit: '2mb' }));
app.use(compression());

// Recuperando informações de rede do usuário.
app.use(requestIp.mw());


require('./middlewares')(app);

app.use('/', require('./routes'));

module.exports = app;
