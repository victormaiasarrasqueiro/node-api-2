const config = {
  env: 'default',
  port: 8070,
  db: {
    debug: true,
    uri: 'mongodb://localhost:27017/lojadb',
  },
  dbmysql: {
    debug: false,
    user: 'adm',
    password: 'adm123456',
    host: 'localhost',
    port: 3306,
    database: 'bancodb_new',
    timeout: 40000,
    connectTimeout: 30000,
    pool: 15,
    max: 15,
    min: 5,
    idle: 10000,
  },
  mock: {
    port: 8095,
  },
  docs: {
    port: 8096,
  },
  captcha: {
    secret: '6Lc_8B4UAAAAALK-JqvGmCg_QCopIY9EKv6T13Ol',
    enabled: false,
  },
  jwt: {
    secret: 'w!it@Lti',
    enabled: true,
    expiresIn: 1200,
  },
};

module.exports = config;

