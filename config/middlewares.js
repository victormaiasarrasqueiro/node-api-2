const logger = require('../config/logger');

module.exports = (app) => {
  app.use((req, res, next) => {
    logger.logarInfo(new Date());
    next();
  });
};
