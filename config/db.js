const logger = require('../config/logger');
const mongoo = require('mongoose');

class Database {
  constructor() {
    this.mongoose = mongoo;
  }

  connect(config) {
    const options = {
      user: config.username,
      pass: config.password,
      server: {
        socketOptions: {},
      },
      replset: {
        socketOptions: {},
      },
    };

    if (config.keepAlive) {
      options.server.socketOptions.keepAlive = config.keepAlive;
      options.replset.socketOptions.keepAlive = config.keepAlive;
    }

    if (config.poolSize) {
      options.server.poolSize = config.poolSize;
      options.replset.poolSize = config.poolSize;
    }

    if (config.connectTimeoutMS) {
      options.server.socketOptions.connectTimeoutMS = config.connectTimeoutMS;
      options.replset.socketOptions.connectTimeoutMS = config.connectTimeoutMS;
    }

    let connection = null;

    // connecta se não estiver conectado
    if (![2, 3].includes(this.mongoose.connection.readyState)) {
      connection = this.mongoose.connect(config.uri, options).connection;
      connection.on('connected', () => {
        logger.logarInfo(`Connected on ${config.uri}`);
      });
    } else {
      connection = this.mongoose.connection;
    }

    connection.on('error', (err) => {
      logger.logarInfo('DB error: ', err);
    });
  }
}

module.exports = new Database();
