const con = require('./mysqlConnector');

class DbMysql {
  constructor() {
    this.pool = con.pool;
  }

  getConnection() {
    return this.pool;
  }
}
module.exports = new DbMysql();
