const config = require('../../../config');
const Sequelize = require('sequelize');

const pool = new Sequelize(config.dbmysql.database, config.dbmysql.user, config.dbmysql.password, {
  host: config.dbmysql.host,
  dialect: 'mysql',
  logging: console.log,
  define: {
    timestamps: false,
  },
  pool: {
    max: config.dbmysql.max,
    min: config.dbmysql.min,
    idle: config.dbmysql.idle,
  },
  operatorAliases: false,
});
module.exports.pool = pool;
