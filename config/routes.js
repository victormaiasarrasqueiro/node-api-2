const express = require('express');

const router = express.Router();
const log = require('./logger');
const uuid = require('uuid/v4');

// logando requests e responses
router.all('*', (req, res, next) => {
  const requestId = uuid();
  const responseEnd = res.end;
  const responseWrite = res.write;
  const chunks = [];
  let ip = null;
  const startAt = new Date();
  const possuiSenhaNoRequest = req.body && (req.body.senha || req.body.password);
  const corpo = JSON.parse(JSON.stringify(req.body));

  if (req.ip) {
    ip = req.ip;
  } else if (req._remoteAddress) {
    ip = req._remoteAddress;
  } else if (req.connection && req.connection.remoteAddress) {
    ip = req.connection;
  } else {
    ip = undefined;
  }


  if (possuiSenhaNoRequest) {
    // caso o body possua uma senha, não grava no log
    delete corpo.senha;
    delete corpo.password;
  }

  req.requestId = requestId;

  const request_obj = {
    params: req.params,
    headers: req.headers,
    body: corpo
  };

  const request_msg = `REQUEST ID: [${requestId}] ao BACKEND => Ip [${ip}] chamando caminho [${req.path}] método [${req.method}], data [${new Date().toISOString()}]`;

  log.logarDebug(request_msg, { request: request_obj });

  res.write = function (chunk) {
    chunks.push(chunk);
    responseWrite.apply(res, arguments);
  };

  res.end = function (chunk) {
    if (typeof chunk === 'string') {
      chunk = new Buffer(chunk);
      chunks.push(chunk);
    }

    let body = Buffer.concat(chunks).toString('utf8');

    try {
      body = JSON.parse(body);
    } catch (e) { log.logarDebug(e); }

    const responseObj = {
      headers: res._headers,
      body,
    };
    const responseMsg = `RESPONSE do BACKEND para REQUEST ID: [${requestId}] - URL: [${req.originalUrl}] => Status [${res.statusCode}], Tempo [${(new Date() - startAt)}ms], data [${new Date().toISOString()}]`;

    log.logarDebug(responseMsg, { request: request_obj, response: responseObj });

    responseEnd.apply(res, arguments);
  };

  next();
});

Helper.forEachModuleFileType('routes', (entry, file_name) => {
  router.use(`/api/${entry}`, require(file_name));
});

router.get('/', (req, res) => { res.status(200).send('Applicação está no ar!'); });

router.use((req, res) => { res.status(404).send('not found!'); });


module.exports = router;
