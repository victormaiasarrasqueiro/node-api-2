module.exports = Object.freeze({
  CONNECTION: 'singletonConnection',
  CODE_200: 200,
  CODE_400: 400,
  CODE_500: 500,
  SUCCESS: 'Successful',
  ERROR: 'Internal Server Error',
  INVALID: 'Bad Request',
})
;
