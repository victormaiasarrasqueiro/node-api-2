require('./helpers');
const config = require('./index');
const db = require('./db');

db.connect(config.db);

Helper.forEachModuleFilePattern('(test|tests/(.*)test)$', (entry, path) => {
  if (!path.includes('integration')) {
    require(path);
  }
});
