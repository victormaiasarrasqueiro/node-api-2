const fs = require('fs');

const Helper = {};

Helper.rootPath = function () {
  return `${__dirname}/../`;
};

Helper.configPath = function () {
  return __dirname;
};

Helper.config = function () {
  return require(Helper.configPath());
};

Helper.forEachModuleFileType = function (fileType, cb) {
  const config = Helper.config();
  let path = config.path.modules;
  let  currentPath;
  let fileName;
  let extension = 'js';

  if (fileType instanceof Array) {
    if (fileType.length > 1) {
      extension = fileType[1];
    }
    fileType = fileType[0];
  }

  fs.readdirSync(path).filter((entry) => {
    // Para cada pasta interna a modules
    currentPath = [path, entry].join('/');
    if (fs.statSync(currentPath).isDirectory()) {
      fileName = [currentPath, `${fileType}.${extension}`].join('/');
      if (fs.existsSync(fileName)) {
        cb(entry, fileName);
      }
    }
  });
};

Helper.forEachModuleFilePattern = function (pattern, cb) {
  const cpattern = new RegExp(pattern);
  const config = Helper.config();
  let path = config.path.modules;
  let  currentPath;
  const extension = 'js';
  if (typeof cb !== 'function') {
    return;
  }
  var recursivelyFindFiles = function (path) {
    fs.readdirSync(path).forEach((entry) => {
      // Para cada pasta interna a modules
      currentPath = [path, entry].join('/');

      if (fs.statSync(currentPath).isDirectory()) {
        recursivelyFindFiles(currentPath);
      } else {
        const currentPathParts = currentPath.split('.');
        const currentPathExtension = currentPathParts.pop();
        const currentPathName = currentPathParts.join('.');

        if (currentPathExtension == extension && currentPathName.match(cpattern)) {
          cb(entry, currentPath);
        }
      }
    });
  };

  recursivelyFindFiles(path);
};

global.Helper = Helper;
